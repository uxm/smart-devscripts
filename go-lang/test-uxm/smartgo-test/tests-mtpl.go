
// GO Lang :: SmartGo/Tests :: Smart.Go.Framework
// (c) 2020-2023 unix-world.org
// r.20230517.1024 :: STABLE

package main

import (
	"os"
	"log"
	"fmt"
	"time"
	"strings"

	smart "github.com/unix-world/smartgo"
	uid   "github.com/unix-world/smartgo/uuid"

	b32 "github.com/unix-world/smartgo/base32"
	b36 "github.com/unix-world/smartgo/base36"
	b58 "github.com/unix-world/smartgo/base58"
	b62 "github.com/unix-world/smartgo/base62"
	b85 "github.com/unix-world/smartgo/base85"
	b92 "github.com/unix-world/smartgo/base92"
)

/*
const (
	THE_TPL string = `[%%%COMMENT%%%]
This is comment one ...
[%%%/COMMENT%%%]
Hallo, this is Markers TPL:[%%%|N%%%][###MARKER|json###][%%%|T%%%][###MARKER2|url|html###][%%%|T%%%][###MARKER2|js###][%%%|T%%%][###MARKER3|idtxt###] [###MARKER3|hex###] [###MARKER3|b64###]
[%%%COMMENT%%%]
This is another comment ...
[%%%/COMMENT%%%]
[%%%IF:TEST1:==one;%%%]
M1: [###MARKER|html###]
[%%%/IF:TEST1%%%]
[%%%IF:TEST2:!=one;%%%]
M2: [###MARKER2|html###]
[%%%ELSE:TEST2%%%]
M1: [###MARKER|html###]
[%%%/IF:TEST2%%%]
[%%%IF:TEST3:==;%%%][###MARKER3|slug|substr10###][%%%/IF:TEST3%%%]
[###MARKER3|slug###]
Dec4: [###MARKER4|dec4###]
Dec3: [###MARKER4|dec3###]
Dec2: [###MARKER4|dec2###]
Dec1: [###MARKER4|dec1###]
Num: [###MARKER4|num###]
Int: [###MARKER4|int###]
Dec4: [###MARKER5|dec4###]
Dec3: [###MARKER5|dec3###]
Dec2: [###MARKER5|dec2###]
Dec1: [###MARKER5|dec1###]
Num: [###MARKER5|num###]
Int: [###MARKER5|int###]
#END
`
)
*/


const (
	THE_TPL string = `[%%%COMMENT%%%]
This is comment one ...
[%%%/COMMENT%%%]
Hallo, this is Markers TPL:[%%%|N%%%][###MARKER|json###][%%%|T%%%][###MARKER2|url|html###][%%%|T%%%][###MARKER2|js###][%%%|T%%%][###MARKER3|idtxt###] [###MARKER3|hex###] [###MARKER3|b64###]
[%%%COMMENT%%%]
This is another comment ...
[%%%/COMMENT%%%]
[%%%IF:NUM-TWO:>100;(8)%%%][%%%IF:NUM-ONE:<101;(8)%%%][%%%IF:NUM-TWO:>=100;(7)%%%][%%%IF:NUM-ONE:<=101;(7)%%%][%%%IF:TEST1:==one;(1)%%%][%%%IF:TEST1:!=two;(2)%%%][%%%IF:MARKER.a:==2;%%%]
M1: [###MARKER|html###]
[%%%/IF:MARKER.a%%%][%%%/IF:TEST1(2)%%%][%%%/IF:TEST1(1)%%%][%%%/IF:NUM-ONE(7)%%%][%%%/IF:NUM-TWO(7)%%%][%%%/IF:NUM-ONE(8)%%%][%%%/IF:NUM-TWO(8)%%%]
[%%%IF:TEST2:!=one;%%%]
M2: [###MARKER2|html###]
[%%%ELSE:TEST2%%%]
M1: [###MARKER|html###]
[%%%/IF:TEST2%%%]
[%%%IF:MARKER.d.1:==d.2;%%%]
M3: [###MARKER|json###]
[%%%/IF:MARKER.d.1%%%]
[%%%IF:MARKER.e.e1:==e.2;%%%]
M4: [###MARKER|json###]
[%%%/IF:MARKER.e.e1%%%]
[%%%IF:TEST3:==;%%%][###MARKER3|slug|substr10###][%%%/IF:TEST3%%%]
[###MARKER3|slug###]
Dec4: [###MARKER4|dec4###]
Dec3: [###MARKER4|dec3###]
Dec2: [###MARKER4|dec2###]
Dec1: [###MARKER4|dec1###]
Num: [###MARKER4|num###]
Int: [###MARKER4|int###]
Dec4: [###MARKER5|dec4###]
Dec3: [###MARKER5|dec3###]
Dec2: [###MARKER5|dec2###]
Dec1: [###MARKER5|dec1###]
Num: [###MARKER5|num###]
Int: [###MARKER5|int###]
#END
`

	THE_JSON string = `{"a":2, "b":"\n<c d=\"About:https://a.b?d=1&c=2\">", "c":"aA-șȘțȚâÂăĂîÎ_+100.12345678901", "d":["d.1","d.2"], "e":{"e0":"e.1","e1":"e.2"}}`

)


func LogToConsoleWithColors() {
	//--
	smart.ClearPrintTerminal()
	//--
//	smart.LogToStdErr("DEBUG")
	smart.LogToConsole("DEBUG", true) // colored or not
//	smart.LogToFile("DEBUG", "logs/", "json", true, true) // json | plain ; also on console ; colored or not
	//--
	log.Println("[DEBUG] Debugging")
	log.Println("[DATA] Data")
	log.Println("[NOTICE] Notice")
	log.Println("[INFO] Info")
	log.Println("[WARNING] Warning")
	log.Println("[ERROR] Error")
	log.Println("[OK] OK")
	log.Println("A log message, with no type")
	//--
} //END FUNCTION


func fatalError(logMessages ...interface{}) {
	//--
	log.Fatal("[ERROR] ", logMessages) // standard logger
	os.Exit(1)
	//--
} //END FUNCTION


func testDTimeNow() {

	dTimeStr := ""

	dtObjUtc := smart.DateTimeStructUtc(dTimeStr)
	theTestJson := smart.JsonEncode(dtObjUtc)
	fmt.Println("Date Obj Json (UTC):", theTestJson)
	if(dtObjUtc.Status != "OK") {
		fatalError("testDTimeNow ERROR (UTC): " + dtObjUtc.ErrMsg)
	} //end if

	D := smart.JsonObjDecode(theTestJson)
	if(D == nil) {
		fatalError("[ERROR] JSON: Decode Fail")
	} //end if
	keys := []string{"status", "errMsg", "time"} // there are many more, but this is just a test
	for y, k := range keys {
		if(D[k] == nil) {
			log.Println("[ERROR] JSON: Key is missing: #" + smart.ConvertIntToStr(y) + " @ " + k)
			return;
		}
	}
	log.Println("[DATA] JSON Decode Keys:", "status="+D["status"].(string), "errMsg="+D["errMsg"].(string), "time="+smart.ConvertJsonNumberToStr(D["time"])) // smart.ConvertInt64ToStr(int64(D["time"].(float64)))) // golang general json decode (without a real mapping structure) maps all numbers to float64 automatically ...

	dtObjLoc := smart.DateTimeStructLocal(dTimeStr)
	fmt.Println("Date Obj Json (LOCAL):", smart.JsonEncode(dtObjLoc))
	if(dtObjLoc.Status != "OK") {
		fatalError("testDTimeNow ERROR (LOCAL): " + dtObjLoc.ErrMsg)
	} //end if

} //END FUNCTION


func testDTimeParse() {

	dTimeStr := "2021-03-16 08:04:07 +0300"

	fmt.Println("Input Date:", dTimeStr)

	dtObjUtc := smart.DateTimeStructUtc(dTimeStr)
	fmt.Println("Date Obj Json", smart.JsonEncode(dtObjUtc))
	if(dtObjUtc.Status != "OK") {
		fatalError("testDTimeParse ERROR (UTC): " + dtObjUtc.ErrMsg)
	} //end if

	dtObjLoc := smart.DateTimeStructLocal(dTimeStr)
	fmt.Println("Date Obj Json", smart.JsonEncode(dtObjLoc))
	if(dtObjLoc.Status != "OK") {
		fatalError("testDTimeParse ERROR (LOCAL): " + dtObjLoc.ErrMsg)
	} //end if

} //END FUNCTION


func testAllTrimWhitespaces(input string) {

	input = smart.StrTrimWhitespaces(input)

	if(input == "") {
		fatalError("TRIM Test Input is Empty !")
	} //end if

	var whiteSpaces string = "\r\n\n\t\r\t \x0B \x00"

	var trimTest string = ""

	trimTest = smart.StrTrimWhitespaces(whiteSpaces + input + whiteSpaces)
	fmt.Println("TRIM Test: `" + trimTest + "`")
	if(trimTest != input) {
		fatalError("TRIM Test ERROR !")
	} //end if

	trimTest = smart.StrTrimLeftWhitespaces(whiteSpaces + input)
	fmt.Println("TRIM LEFT Test: `" + trimTest + "`")
	if(trimTest != input) {
		fatalError("LEFT TRIM Test ERROR !")
	} //end if

	trimTest = smart.StrTrimRightWhitespaces(input + whiteSpaces)
	fmt.Println("TRIM RIGHT Test: `" + trimTest + "`")
	if(trimTest != input) {
		fatalError("RIGHT TRIM Test ERROR !")
	} //end if

} //END FUNCTION


func testStrings() {

	var test1Str string = "1234567890 abcdefgh șȘțȚâÂăĂîÎ _ abcdefghijklmnopqrstuvwxyz-ABCDEFGHIJKLMNOPQRSTUVWXYZ:;\"'~`!@#$%^&*()+=[]{}|\\<>,.?/\t\r\n@  abcdefgh șȘțȚâÂăĂîÎ üÜöÖäÄ"

	if(!smart.StrStartsWith(test1Str, "1234567890 abcdefgh șȘțȚâÂăĂîÎ ")) {
		fatalError("SubString Prefix Test FAILED !")
	} //end if
	if(!smart.StrEndsWith(test1Str, "  abcdefgh șȘțȚâÂăĂîÎ üÜöÖäÄ")) {
		fatalError("SubString Suffix Test FAILED !")
	} //end if

	if(smart.StrMBSubstr(test1Str, 0, 0) != smart.StrSubstr(test1Str, 0, 0)) {
		fatalError("SubString ASCII/Unicode Comparison Test FAILED !")
	} //end if

	var needleStr string = ""
	var needleStrExpected string = ""
	var testThePos int = -777

	needleStr = "abcdefgh șȘțȚâÂăĂîÎ"
	needleStrExpected = "Abcdefgh șȘțȚâÂăĂîÎ"
	theTestUcFirst := smart.StrUcFirst(needleStr)
	if(theTestUcFirst != needleStrExpected) {
		fatalError("StrUcFirst Test FAILED. Expected result is ", needleStrExpected, "but get: ", theTestUcFirst)
	}

	needleStr = "abcdefgh șȘțȚâÂăĂîÎ"
	needleStrExpected = "Abcdefgh Șșțțââăăîî"
	theTestUcWords := smart.StrUcWords(needleStr)
	if(theTestUcWords != needleStrExpected) {
		fatalError("StrUcWords Test FAILED. Expected result is ", needleStrExpected, "but get: ", theTestUcWords)
	}

	needleStr = "abcdefgh șȘțȚâÂăĂîÎ"
	testThePos = smart.StrPos(test1Str, needleStr)
	if(testThePos != 11) {
		fatalError("StrPos (case sensitive) Test FAILED. Expected result is 11 but get: ", testThePos)
	} //end if

	needleStr = "abcdefgh șȘțȚÂÂăĂîÎ"
	testThePos = smart.StrPos(test1Str, needleStr)
	if(testThePos != -1) {
		fatalError("StrPos (case sensitive) Test FAILED. Expected result is -1 but get: ", testThePos)
	} //end if

	needleStr = "abcdefgh ȘșțȚâÂăĂîÎ"
	testThePos = smart.StrIPos(test1Str, needleStr)
	if(testThePos != 11) {
		fatalError("StrIPos (case insensitive) Test FAILED. Expected result is 11 but get: ", testThePos)
	} //end if

	needleStr = "abcdefgh  ȘșțȚâÂăĂîÎ"
	testThePos = smart.StrIPos(test1Str, needleStr)
	if(testThePos != -1) {
		fatalError("StrIPos (case insensitive) Test FAILED. Expected result is -1 but get: ", testThePos)
	} //end if

	needleStr = "abcdefgh șȘțȚâÂăĂîÎ"
	testThePos = smart.StrRPos(test1Str, needleStr)
	if(testThePos != 122) {
		fatalError("StrRPos (case sensitive) Test FAILED. Expected result is 122 but get: ", testThePos)
	} //end if

	needleStr = "abcdefgh șȘțȚÂÂăĂîÎ"
	testThePos = smart.StrRPos(test1Str, needleStr)
	if(testThePos != -1) {
		fatalError("StrRPos (case sensitive) Test FAILED. Expected result is -1 but get: ", testThePos)
	} //end if

	needleStr = "abcdefgh ȘșțȚâÂăĂîÎ"
	testThePos = smart.StrRIPos(test1Str, needleStr)
	if(testThePos != 122) {
		fatalError("StrRIPos (case insensitive) Test FAILED. Expected result is 122 but get: ", testThePos)
	} //end if

	needleStr = "abcdefgh  ȘșțȚâÂăĂîÎ"
	testThePos = smart.StrRIPos(test1Str, needleStr)
	if(testThePos != -1) {
		fatalError("StrRIPos (case insensitive) Test FAILED. Expected result is -1 but get: ", testThePos)
	} //end if

	fmt.Println("Strings SubStr Tests Result: PASSED")

} //END FUNCTION


func testNetFx() {
	//--
	var validIPv4 string = "127.0.0.1"
	var validIPv6Long  string = "0:0:0:0:0:0:0:1"
	var validIPv6Short string = "::1"
	var validHostNameLocal string = "localhost"
	var validHostNameInet string = "some-internet_domain.ext"
	var validPort string = "8888"
	//--
	if(!smart.IsNetValidIpAddr(validIPv4)) {
		fatalError("Net Validation Test Ipv4 failed with:", validIPv4)
	} //end if
	if(smart.IsNetValidIpAddr(validIPv4 + ".3")) {
		fatalError("Net Validation Test Ipv4 failed with wrong IP:", validIPv4 + ".3")
	} //end if
	//--
	if(!smart.IsNetValidIpAddr(validIPv6Long)) {
		fatalError("Net Validation Test Ipv6-Long failed with:", validIPv6Long)
	} //end if
	if(smart.IsNetValidIpAddr(validIPv6Long + ":3")) {
		fatalError("Net Validation Test Ipv6-Long failed with wrong IP:", validIPv6Long + ":3")
	} //end if
	//--
	if(!smart.IsNetValidIpAddr(validIPv6Short)) {
		fatalError("Net Validation Test Ipv6-Short failed with:", validIPv6Short)
	} //end if
	if(smart.IsNetValidIpAddr(validIPv6Short + "::3")) {
		fatalError("Net Validation Test Ipv6-Short failed with wrong IP:", validIPv6Short + "::3")
	} //end if
	//--
	if((smart.IsNetValidIpAddr("")) || (smart.IsNetValidIpAddr("0.0.0.0"))) {
		fatalError("Net Validation Test Ipv4 failed with empty IP")
	} //end if
	if((smart.IsNetValidIpAddr("::0")) || (smart.IsNetValidIpAddr("::"))) {
		fatalError("Net Validation Test Ipv6 failed with empty IP")
	} //end if
	//--
	if(!smart.IsNetValidHostName(validHostNameLocal)) {
		fatalError("Net Validation Test LocalHost failed with:", validHostNameLocal)
	} //end if
	if(!smart.IsNetValidHostName(validHostNameInet)) {
		fatalError("Net Validation Test InetHost failed with:", validHostNameInet)
	} //end if
	//--
	if(!smart.IsNetValidPortNum(smart.ParseStrAsInt64(validPort))) {
		fatalError("Net Validation Test ValidPort (int64) failed with:", validPort)
	} //end if
	//--
	if(!smart.IsNetValidPortStr(validPort)) {
		fatalError("Net Validation Test ValidPort (string) failed with:", validPort)
	} //end if
	if(smart.IsNetValidPortStr(validPort + "8")) {
		fatalError("Net Validation Test ValidPort (string) failed with wrong Port:", validPort + "8")
	} //end if
	if(smart.IsNetValidPortStr(validPort + ":")) {
		fatalError("Net Validation Test ValidPort (string) failed with wrong Port:", validPort + ":")
	} //end if
	if(smart.IsNetValidPortStr(":" + validPort)) {
		fatalError("Net Validation Test ValidPort (string) failed with wrong Port:", ":" + validPort)
	} //end if
	//--
	fmt.Println("Net Validation Tests Result: PASSED")
	//--
} //END FUNCTION


func testFileSystem(input string) {

	var thePath string = "a/path/to/some/file.ext"

	var pathRegex string = smart.REGEX_SMART_SAFE_PATH_NAME
	var pathFileRegex string = smart.REGEX_SMART_SAFE_FILE_NAME

	fmt.Println("------------------------- Path Get BaseName / DirName TESTS -------------------------")

	if(!smart.StrRegexMatchString(pathRegex, thePath)) {
		fatalError("Path Regex `" + pathRegex + "` Match Failed: `" + thePath + "`")
	} //end if

	fBaseExt := smart.PathBaseExtension(thePath)
	if(fBaseExt != ".ext") {
		fatalError("Path Get BaseExtension FAILED ; BaseExtension = `" + fBaseExt + "` ; Path = `" + thePath + "`")
	} //end if
	if(!smart.StrRegexMatchString(pathFileRegex, fBaseExt)) {
		fatalError("File Name Ext Regex `" + pathFileRegex + "` Match Failed: `" + fBaseExt + "`")
	} //end if

	fBaseName := smart.PathBaseName(thePath)
	if(fBaseName != "file.ext") {
		fatalError("Path Get BaseName FAILED ; BaseName = `" + fBaseName + "` ; Path = `" + thePath + "`")
	} //end if
	if(!smart.StrRegexMatchString(pathFileRegex, fBaseName)) {
		fatalError("File Name Regex `" + pathFileRegex + "` Match Failed: `" + fBaseName + "`")
	} //end if

	fDirName := smart.PathDirName(thePath)
	if(fDirName != "a/path/to/some") {
		fatalError("Path Get DirName FAILED ; DirName = `" + fDirName + "` ; Path = `" + thePath + "`")
	} //end if
	if(!smart.StrRegexMatchString(pathRegex, fDirName)) {
		fatalError("Path (Dir) Regex `" + pathRegex + "` Match Failed: `" + fDirName + "`")
	} //end if

	fmt.Println("Path Get / Regex Check BaseName and DirName OK ; Path = `" + thePath + "` ; BaseName = `" + fBaseName + "` ; DirName = `" + fDirName + "`")

	fmt.Println("------------------------- Absolute Path TESTS -------------------------")

	if(smart.PathIsAbsolute(thePath) == true) {
		fatalError("Absolute Path Detection Failed. This is not an absolute path: " + thePath)
	} //end if
	fmt.Println("Absolute Path Test OK (not absolute path): " + thePath)

	var theAPath string = "/" + thePath
	if(smart.PathIsAbsolute(theAPath) != true) {
		fatalError("Absolute Path Detection Failed. This is an absolute path: " + theAPath)
	} //end if
	fmt.Println("Absolute Path Test OK (it is an absolute path): " + theAPath)

	theAPath = ":" + thePath
	if(smart.PathIsAbsolute(theAPath) != true) {
		fatalError("Absolute Path Detection Failed. This is an absolute path: " + theAPath)
	} //end if
	fmt.Println("Absolute Path Test OK (it is an absolute path): " + theAPath)

	theAPath = "C:" + thePath
	if(smart.PathIsAbsolute(theAPath) != true) {
		fatalError("Absolute Path Detection Failed. This is an absolute path: " + theAPath)
	} //end if
	fmt.Println("Absolute Path Test OK (it is an absolute path): " + theAPath)

	fmt.Println("------------------------- Safe / Valid Path and FileName TESTS -------------------------")

	if(smart.PathIsSafeValidPath(thePath) != true) {
		fatalError("Invalid or Unsafe Path Detection Failed. This is not an invalid unsafe path: " + thePath)
	} //end if
	fmt.Println("Invalid or Unsafe Path Test OK (not invalid unsafe path): " + thePath)

	if((smart.PathIsSafeValidPath(fBaseName) != true) || (smart.PathIsSafeValidFileName(fBaseName) != true)) {
		fatalError("Invalid or Unsafe FileName Detection Failed. This is not an invalid unsafe file name: " + fBaseName)
	} //end if
	fmt.Println("Invalid or Unsafe FileName Test OK (not invalid unsafe file name): " + fBaseName)

	fmt.Println("------------------------- Backward Unsafe Path TESTS -------------------------")

	if(smart.PathIsBackwardUnsafe(thePath) == true) {
		fatalError("Backward Unsafe Path Detection Failed. This is not a backward unsafe path: " + thePath)
	} //end if
	fmt.Println("Backward Unsafe Path Test OK (not backward unsafe path): " + thePath)

	theAPath = thePath + "/../"
	if(smart.PathIsBackwardUnsafe(theAPath) != true) {
		fatalError("Backward Unsafe Path Detection Failed. This is a backward unsafe path: " + theAPath)
	} //end if
	fmt.Println("Backward Unsafe Path Test OK (it is a backward unsafe path): " + theAPath)

	theAPath = thePath + "/./"
	if(smart.PathIsBackwardUnsafe(theAPath) != true) {
		fatalError("Backward Unsafe Path Detection Failed. This is a backward unsafe path: " + theAPath)
	} //end if
	fmt.Println("Backward Unsafe Path Test OK (it is a backward unsafe path): " + theAPath)

	theAPath = thePath + "/.."
	if(smart.PathIsBackwardUnsafe(theAPath) != true) {
		fatalError("Backward Unsafe Path Detection Failed. This is a backward unsafe path: " + theAPath)
	} //end if
	fmt.Println("Backward Unsafe Path Test OK (it is a backward unsafe path): " + theAPath)

	theAPath = thePath + "../"
	if(smart.PathIsBackwardUnsafe(theAPath) != true) {
		fatalError("Backward Unsafe Path Detection Failed. This is a backward unsafe path: " + theAPath)
	} //end if
	fmt.Println("Backward Unsafe Path Test OK (it is a backward unsafe path): " + theAPath)

	fmt.Println("------------------------- Path/File Exists TESTS -------------------------")

	if(smart.PathExists("./nonexisting") == true) {
		fatalError("Errors encountered while testing a non-existing path exists")
	} //end if
	if(smart.PathIsDir("./nonexisting") == true) {
		fatalError("Errors encountered while testing a non-existing path is a dir")
	} //end if
	if(smart.PathIsFile("./nonexisting") == true) {
		fatalError("Errors encountered while testing a non-existing path is a file")
	} //end if

	if(smart.PathExists("./") != true) {
		fatalError("Errors encountered while testing if the current directory ./ exists")
	} //end if
	if(smart.PathIsDir("./") != true) {
		fatalError("Errors encountered while testing if the current directory ./ is a dir")
	} //end if
	if(smart.PathIsFile("./") == true) {
		fatalError("Errors encountered while testing if the current directory ./ is not a file")
	} //end if

	var crrBinGoFile string = smart.PathGetCurrentExecutableName()
	if(crrBinGoFile == "") {
		fatalError("FAILED to get the current GO executable Name")
	} //end if
	if(smart.PathExists(crrBinGoFile + ".go") == true) {
		crrBinGoFile = crrBinGoFile + ".go"
	}
	fmt.Println("Current Executable Name: `" + crrBinGoFile + "`")
	if(smart.PathExists(crrBinGoFile) != true) {
		fatalError("Errors encountered while testing if the current executable file exists:", crrBinGoFile)
	} //end if
	if(smart.PathIsFile(crrBinGoFile) != true) {
		fatalError("Errors encountered while testing if the current executable file is a file:", crrBinGoFile)
	} //end if
	if(smart.PathIsDir(crrBinGoFile) == true) {
		fatalError("Errors encountered while testing if the current executable file is not a dir:", crrBinGoFile)
	} //end if

	var crrBinGoDir string = smart.PathGetCurrentExecutableDir()
	if(crrBinGoDir == "") {
		fatalError("FAILED to get the current GO executable Dir")
	} //end if
	fmt.Println("Current Executable Dir: `" + crrBinGoDir + "`")
	if(smart.PathExists(crrBinGoDir) != true) {
		fatalError("Errors encountered while testing if the current executable dir exists:", crrBinGoDir)
	} //end if
	if(smart.PathIsFile(crrBinGoDir) == true) {
		fatalError("Errors encountered while testing if the current executable dir is not a file:", crrBinGoDir)
	} //end if
	if(smart.PathIsDir(crrBinGoDir) != true) {
		fatalError("Errors encountered while testing if the current executable dir is a dir:", crrBinGoDir)
	} //end if

	fmt.Println("Path Exists Test Result: PASSED")

	fmt.Println("------------------------- Dir Create and Rename TESTS -------------------------")

	var theBaseDir string = "test-Dir/"
	var theDir string = theBaseDir

	if(smart.PathExists(theDir)) {
		fatalError("Dir Tests EXISTS. Remove it manually before starting the tests: `" + theDir)
	} //end if

	isOkDirCreate1, errmsg := smart.SafePathDirCreate(theDir, false, false)
	if(errmsg != "") {
		fatalError("Errors encountered while creating the test dir `" + theDir + "`:", errmsg)
	} //end if
	if(isOkDirCreate1 != true) {
		fatalError("Failed to create the test dir (level 1) `" + theDir + "` (result is FALSE)")
	} //end if
	fmt.Println("Test PASSED ; DirCreate: `" + theDir + "` ; Result =", isOkDirCreate1)

	dirMTime, errmsg := smart.SafePathGetMTime(theDir, false)
	if(errmsg != "") {
		fatalError("Errors encountered while trying to get Dir MTime `" + theDir + "`:", errmsg)
	} //end if
	fmt.Println("Test PASSED ; Dir MTime: `" + theDir + "` ; Result =", dirMTime, "; Now=", int64(time.Now().Unix()))

	var theRenamedDir string = theDir + "LEVEL2/level3-X/"
	theDir = theDir + "LEVEL2/level3/"
	isOkDirCreate2, errmsg := smart.SafePathDirCreate(theDir, false, false)
	if((errmsg == "") || (isOkDirCreate2 == true)) {
		fatalError("Errors encountered while creating the recursive test dir (levels ++) without allowed (should return error but was not) `" + theDir + "`: ErrMsg=", errmsg, " ; isSuccess=", isOkDirCreate2)
	} //end if
	fmt.Println("Test PASSED ; DirCreateRecursive/Dissalowed: `" + theDir + "` ; Result =", isOkDirCreate2)

	isOkDirCreate3, errmsg := smart.SafePathDirCreate(theDir, true, false)
	if(errmsg != "") {
		fatalError("Errors encountered while creating the test recursive dir (levels ++) `" + theDir + "`:", errmsg)
	} //end if
	if(isOkDirCreate3 != true) {
		fatalError("Failed to create the test recursive dir (levels ++) `" + theDir + "` (result is FALSE)")
	} //end if
	fmt.Println("Test PASSED ; DirCreateRecursive: `" + theDir + "` ; Result =", isOkDirCreate3)

	isOkRenameDir, errmsg := smart.SafePathDirRename(theDir, theRenamedDir, false)
	if(errmsg != "") {
		fatalError("Errors encountered while renaming the test dir `" + theDir + "` to `" + theRenamedDir + "`:", errmsg)
	} //end if
	if(isOkRenameDir != true) {
		fatalError("Failed to rename the test dir `" + theDir + "` to `" + theRenamedDir + "` (result is FALSE)")
	} //end if
	fmt.Println("Test PASSED ; DirRename: `" + theDir + "` to `" + theRenamedDir + "` ; Result =", isOkRenameDir)
	theDir = theRenamedDir

	if((!smart.PathExists(theDir)) || (!smart.PathIsDir(theDir))) {
		fatalError("Dir Tests FAILED. Cannot find the Path/Dir: `" + theDir)
	} //end if
	fmt.Println("Tests PASSED ; DirCreate/DirRename [ +/- Recursive ]: `" + theDir + "` ; ALL")

	fmt.Println("------------------------- File Write TESTS -------------------------")

	var theTestFile string = theBaseDir + "test-FILE.txt"
	var theFContents string = input + "\n" + smart.DateNowLocal() + "\n"

	var theEmptyTestFile string = theTestFile + ".empty.TXT"
	isESuccess, errmsg := smart.SafePathFileWrite(theEmptyTestFile, "w", false, "")
	if(errmsg != "") {
		fatalError("Errors encountered while writing the empty test file `" + theEmptyTestFile + "`:", errmsg)
	} //end if
	if(isESuccess != true) {
		fatalError("Failed to write the empty test file `" + theEmptyTestFile + "` (result is FALSE)")
	} //end if
	fmt.Println("Test PASSED ; WriteEmptyFile: `" + theEmptyTestFile + "` ; Result =", isESuccess)

	isSuccess, errmsg := smart.SafePathFileWrite(theTestFile, "w", false, theFContents)
	if(errmsg != "") {
		fatalError("Errors encountered while writing the test file `" + theTestFile + "`:", errmsg)
	} //end if
	if(isSuccess != true) {
		fatalError("Failed to write the test file `" + theTestFile + "` (result is FALSE)")
	} //end if
	fmt.Println("Test PASSED ; WriteFile: `" + theTestFile + "` ; Result =", isSuccess)

	fmt.Println("------------------------- File Write/Append TESTS -------------------------")

	isASuccess, errmsg := smart.SafePathFileWrite(theTestFile, "a", false, theFContents)
	if(errmsg != "") {
		fatalError("Errors encountered while write/append the test file `" + theTestFile + "`:", errmsg)
	} //end if
	if(isASuccess != true) {
		fatalError("Failed to write/append the test file `" + theTestFile + "` (result is FALSE)")
	} //end if
	fmt.Println("Test PASSED ; Append/WriteFile: `" + theTestFile + "` ; Result =", isASuccess)

	fmt.Println("------------------------- File Read TESTS -------------------------")

	fMd5Sum, errmsg := smart.SafePathFileMd5(theEmptyTestFile, false)
	if(errmsg != "") {
		fatalError("Errors encountered while making the MD5 sum of file: `" + theEmptyTestFile + "`", errmsg)
	} //end if
	if(fMd5Sum != "d41d8cd98f00b204e9800998ecf8427e") { // compare it with the md5 of empty string, should match ! (this is because the file contents is empty)
		fatalError("The MD5 sum of file: `" + theEmptyTestFile + "` is INVALID: `" + fMd5Sum + "`")
	} //end if

	fSha1Sum, errmsg := smart.SafePathFileSha("sha1", theEmptyTestFile, false)
	if(errmsg != "") {
		fatalError("Errors encountered while making the SHA1 sum of file: `" + theEmptyTestFile + "`", errmsg)
	} //end if
	if(fSha1Sum != "da39a3ee5e6b4b0d3255bfef95601890afd80709") { // compare it with the sha1 of empty string, should match ! (this is because the file contents is empty)
		fatalError("The SHA1 sum of file: `" + theEmptyTestFile + "` is INVALID: `" + fSha1Sum + "`")
	} //end if

	fSha256Sum, errmsg := smart.SafePathFileSha("sha256", theEmptyTestFile, false)
	if(errmsg != "") {
		fatalError("Errors encountered while making the SHA1 sum of file: `" + theEmptyTestFile + "`", errmsg)
	} //end if
	if(fSha256Sum != "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855") { // compare it with the sha1 of empty string, should match ! (this is because the file contents is empty)
		fatalError("The SHA256 sum of file: `" + theEmptyTestFile + "` is INVALID: `" + fSha256Sum + "`")
	} //end if

	fSha512Sum, errmsg := smart.SafePathFileSha("sha512", theEmptyTestFile, false)
	if(errmsg != "") {
		fatalError("Errors encountered while making the SHA1 sum of file: `" + theEmptyTestFile + "`", errmsg)
	} //end if
	if(fSha512Sum != "cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e") { // compare it with the sha1 of empty string, should match ! (this is because the file contents is empty)
		fatalError("The SHA512 sum of file: `" + theEmptyTestFile + "` is INVALID: `" + fSha512Sum + "`")
	} //end if

	fEmptyContent, errmsg := smart.SafePathFileRead(theEmptyTestFile, false)
	if(errmsg != "") {
		fatalError("Errors encountered while reading the empty test file `" + theEmptyTestFile + "`:", errmsg)
	} //end if
	if(fEmptyContent != "") {
		fatalError("Failed to read the empty test file `" + theEmptyTestFile + "` (not empty content)")
	} //end if
	fmt.Println("Test PASSED ; ReadEmptyFile `" + theEmptyTestFile + "` ; FileLength =", len(fEmptyContent), "bytes")

	fContent, errmsg := smart.SafePathFileRead(theTestFile, false)
	if(errmsg != "") {
		fatalError("Errors encountered while reading the test file `" + theTestFile + "`:", errmsg)
	} //end if
	if(fContent == "") {
		fatalError("Failed to read the test file `" + theTestFile + "` (empty content)")
	} //end if
	if(fContent != (theFContents + theFContents)) {
		fatalError("Failed to read the test file `" + theTestFile + "` (content is wrong): `" + fContent + "` instead of `" + theFContents + "`")
	} //end if
	fmt.Println("Test PASSED ; ReadFile `" + theTestFile + "` ; FileLength =", len(fContent), "bytes")

	fmt.Println("------------------------- File Rename TESTS -------------------------")

	var theNewTestFile string = theDir + "test-Renamed.txt"

	isReSuccess, errmsg := smart.SafePathFileRename(theTestFile, theNewTestFile, false)
	if(errmsg != "") {
		fatalError("Errors encountered while renaming the test file `" + theTestFile + "` to `" + theNewTestFile + "`:", errmsg)
	} //end if
	if(isReSuccess != true) {
		fatalError("Failed to rename the test file `" + theTestFile + "` to `" + theNewTestFile + "` (result is FALSE)")
	} //end if
	if((smart.PathExists(theTestFile) != false) || (smart.PathIsFile(theTestFile) != false) || (smart.PathIsDir(theTestFile) != false)) {
		fatalError("Failed to rename the test file `" + theTestFile + "` to `" + theNewTestFile + "` (old path still exists after rename ...)")
	} //end if
	if((smart.PathExists(theNewTestFile) != true) || (smart.PathIsFile(theNewTestFile) != true)) {
		fatalError("Failed to rename the test file `" + theTestFile + "` to `" + theNewTestFile + "` (new file does not exists after rename ...)")
	} //end if
	fmt.Println("Test PASSED ; RenameFile: `" + theTestFile + "` to `" + theNewTestFile + "` ; Result =", isReSuccess)

	fmt.Println("------------------------- File Copy TESTS -------------------------")

	theCopyEmptyTestFile := theEmptyTestFile + "_copied-from-ORIGINAL-EMPTY_File-here.txt"
	isECpSuccess, errmsg := smart.SafePathFileCopy(theEmptyTestFile, theCopyEmptyTestFile, false)
	if(errmsg != "") {
		fatalError("Errors encountered while copying the empty test file `" + theEmptyTestFile + "` to `" + theCopyEmptyTestFile + "`:", errmsg)
	} //end if
	if(isECpSuccess != true) {
		fatalError("Failed to copy the empty test file `" + theEmptyTestFile + "` to `" + theCopyEmptyTestFile + "` (result is FALSE)")
	} //end if
	fmt.Println("Test PASSED ; CopyEmptyFile: `" + theEmptyTestFile + "` to `" + theCopyEmptyTestFile + "` ; Result =", isECpSuccess)

	theCopyTestFile := theBaseDir + "test-renamed-from-ORIGINAL-and-after-Copied-here.txt"
	isCpSuccess, errmsg := smart.SafePathFileCopy(theNewTestFile, theCopyTestFile, false)
	if(errmsg != "") {
		fatalError("Errors encountered while copying the test file `" + theNewTestFile + "` to `" + theCopyTestFile + "`:", errmsg)
	} //end if
	if(isCpSuccess != true) {
		fatalError("Failed to copy the test file `" + theNewTestFile + "` to `" + theCopyTestFile + "` (result is FALSE)")
	} //end if
	fmt.Println("Test PASSED ; CopyFile: `" + theNewTestFile + "` to `" + theCopyTestFile + "` ; Result =", isCpSuccess)

	var theScanDir string = theBaseDir
	fmt.Println("------------------------- Dir Scan (Non-Recursive) TESTS: `" + theScanDir + "` -------------------------")
	testScanDir(theScanDir, false)
	fmt.Println("------------------------- Dir Scan (Recursive) TESTS `" + theScanDir + "` -------------------------")
	testScanDir(theScanDir, true)

	fmt.Println("------------------------- File Delete TESTS -------------------------")

	isDSuccess, errmsg := smart.SafePathFileDelete(theNewTestFile, false)
	if(errmsg != "") {
		fatalError("Errors encountered while deleting the test file `" + theNewTestFile + "`:", errmsg)
	} //end if
	if(isDSuccess != true) {
		fatalError("Failed to delete the test file `" + theNewTestFile + "` (result is FALSE)")
	} //end if
	if(smart.PathExists(theNewTestFile) != false) {
		fatalError("Failed to delete the test file `" + theNewTestFile + "` (path still exists after delete ...)")
	} //end if
	fmt.Println("Test PASSED ; DeleteFile: `" + theNewTestFile + "` ; Result =", isDSuccess)

	fmt.Println("------------------------- Copied File Delete TESTS -------------------------")

	isDCSuccess, errmsg := smart.SafePathFileDelete(theCopyTestFile, false)
	if(errmsg != "") {
		fatalError("Errors encountered while deleting the copied test file `" + theCopyTestFile + "`:", errmsg)
	} //end if
	if(isDCSuccess != true) {
		fatalError("Failed to delete the copied test file `" + theCopyTestFile + "` (result is FALSE)")
	} //end if
	if(smart.PathExists(theCopyTestFile) != false) {
		fatalError("Failed to delete the copied test file `" + theCopyTestFile + "` (path still exists after delete ...)")
	} //end if
	fmt.Println("Test PASSED ; DeleteCopiedFile: `" + theCopyTestFile + "` ; Result =", isDCSuccess)

	fmt.Println("------------------------- File Delete Non-Existing TESTS -------------------------")

	theTestFile = theTestFile + "-nonexisting"

	isDNSuccess, errmsg := smart.SafePathFileDelete(theTestFile, false)
	if(errmsg != "") {
		fatalError("Errors encountered while deleting (non-existing) the test file `" + theTestFile + "`:", errmsg)
	} //end if
	if(isDNSuccess != true) {
		fatalError("Failed to delete (non-existing) the test file `" + theTestFile + "` (result is FALSE)")
	} //end if
	fmt.Println("Test PASSED ; DeleteFile (non-existing): `" + theTestFile + "` ; Result =", isDNSuccess)

	fmt.Println("------------------------- Dir Delete TESTS -------------------------")

	isOkDirDeleteFinal, errmsg := smart.SafePathDirDelete(theBaseDir, false)
	if(errmsg != "") {
		fatalError("Errors encountered while deleting the test dir `" + theBaseDir + "`:", errmsg)
	} //end if
	if(isOkDirDeleteFinal != true) {
		fatalError("Failed to delete the test dir `" + theBaseDir + "` (result is FALSE)")
	} //end if
	fmt.Println("Test PASSED ; DirDelete: `" + theBaseDir + "` ; Result =", isOkDirDeleteFinal)

	if(smart.PathExists(theBaseDir)) {
		fatalError("Dir Tests EXISTS after delete: `" + theBaseDir)
	} //end if

	isOkDirDeleteNonExisting, errmsg := smart.SafePathDirDelete(theBaseDir, false)
	if(errmsg != "") {
		fatalError("Errors encountered while deleting the non-existing test dir `" + theBaseDir + "`:", errmsg)
	} //end if
	if(isOkDirDeleteNonExisting != true) {
		fatalError("Failed to delete the non-existing test dir `" + theBaseDir + "` (result is FALSE)")
	} //end if
	fmt.Println("Test PASSED ; DirNonExistingDelete: `" + theBaseDir + "` ; Result =", isOkDirDeleteNonExisting)

	fmt.Println("------------------------- Current Dir Get Absolute Path TESTS -------------------------")

	var theAbsoluteCurrentPath string = smart.PathGetAbsoluteFromRelative("./")
	fmt.Println("Current Absolute Path of current dir `./` is: `" +  theAbsoluteCurrentPath + "`")


} //END FUNCTION


func testScanDir(theScanDir string, scanRecursive bool) {
	//--
	scanOk, errScanMsg, arrDirs, arrFiles := smart.SafePathDirScan(theScanDir, scanRecursive, false)
	if((scanOk != true) || (errScanMsg != "")) {
		fatalError("Failed to recursive scan the `" + theScanDir + "` ( result is:", scanOk, " ; errMsg is:", errScanMsg, ")")
	} //end if
	for _, dir := range arrDirs {
		fmt.Println("DIR: `" + dir + "`")
	} //end for
	for _, file := range arrFiles {
		fmt.Println("FILE: `" + file + "`")
	} //end for
	//--
} //END FUNCTION


func testExecProgr() {

	/*
	var inputStr = "abc:"
	for i:=0; i<10000; i++ {
		inputStr += smart.ConvertIntToStr(i) + ","
	}
	isSuccess, outStd, errStd := smart.ExecCmd("output", "output", "", inputStr, "cat")
	*/

	isSuccess, outStd, errStd := smart.ExecCmd("output", "output", "", "", "ping", "-c 5", "unix-world.org")
	if(isSuccess != true) {
		fatalError("ERROR: ExecCmd", "StdOut:\n`", outStd, "`\n", "StdErr:\n`", errStd, "`\n")
	} //end if
	fmt.Println("OK: ExecCmd", "StdOut:\n`", outStd, "`\n", "StdErr:\n`", errStd, "`\n")

} //END FUNCTION


func testExecTimedProgr() {

	isSuccess, outStd, errStd := smart.ExecTimedCmd(3, "output", "output", "", "", "ping", "-c 25", "yahoo.com")
	if((isSuccess != true) && (smart.StrContains(errStd, smart.CMD_EXEC_ERR_SIGNATURE + " signal: killed"))) {
		fmt.Println("OK: ExecTimedCmd (3 sec) was ended because Timed out ...", "StdOut:\n`", outStd, "`\n", "StdErr:\n`", errStd, "`\n")
	} else {
		fatalError("ERROR: ExecTimedCmd (3 sec)", "StdOut:\n`", outStd, "`\n", "StdErr:\n`", errStd, "`\n")
	} //end if

} //END FUNCTION


func testJsonDecode() {

testObjJson := `
{
	"test": {
		"string_array": ["asdf", "zxcv"],
		"array": [1, "2", 3],
		"arraywithsubs": [{"subkeyone": 1}],
		"bignum": 9223372036854775807,
		"string": "simplejson",
		"bool": true
	}
}
`

testArrJson := `
[
	"a",
	2,
	"c",
	{ "e": "f", "g": [0, 1, "z"] }
]
`

testStrJson := `"abc"`

	jsonObjData := smart.JsonObjDecode(testObjJson)
	log.Println("[DATA] jsonObjData:", jsonObjData)

	jsonArrData := smart.JsonArrDecode(testArrJson)
	log.Println("[DATA] jsonArrData:", jsonArrData)

	jsonStrData := smart.JsonStrDecode(testStrJson)
	log.Println("[DATA] jsonStrData:", jsonStrData)

} //END FUNCTION


func main() {

	LogToConsoleWithColors()

	var timerStart time.Time
	var input string = "Lorem Ipsum dolor sit Amet"
	var unicodeString string = "Unicode String:		şŞţŢăĂîÎâÂșȘțȚ (05-09#"

	fmt.Println("========================= NUMERIC TESTS =========================")

	var numInt int = 64
	var numInt64 int64 = 64
	var numFloat64 float64 = 64.2

	if(int64(numInt) != numInt64) {
		fatalError("Numeric Tests Failed with numInt vs. numInt64")
	} //end if
	if(uint64(numInt) != uint64(numInt64)) {
		fatalError("Numeric Tests Failed with UINT numInt vs. numInt64")
	} //end if

	var numStrInt string = smart.ConvertIntToStr(numInt)
	var numStrInt64 string = smart.ConvertInt64ToStr(numInt64)
	var numStrFloat64 string = smart.ConvertFloat64ToStr(numFloat64)
	var strStrFloat64Int64 string = smart.ConvertInt64ToStr(smart.ParseStrAsInt64(numStrFloat64))
	if(numStrInt != "64") {
		fatalError("Numeric to String Conversion Tests Failed with numInt:", numStrInt)
	} //end if
	if(numStrInt64 != "64") {
		fatalError("Numeric to String Conversion Tests Failed with numStrInt64:", numStrInt64)
	} //end if
	if(numStrFloat64 != "64.2") {
		fatalError("Numeric to String Conversion Tests Failed with numStrFloat64:", numStrFloat64)
	} //end if
	if(strStrFloat64Int64 != "64") {
		fatalError("Numeric to String Conversion Tests Failed with numStrFloat64 -> Int64:", numStrFloat64, strStrFloat64Int64)
	} //end if

	var numParsedInt int = int(smart.ParseStrAsInt64(numStrInt))
	var numParsedInt64 int64 = smart.ParseStrAsInt64(numStrInt64)
	if(numParsedInt != 64) {
		fatalError("String to Numeric Conversion Tests Failed with numParsedInt")
	} //end if
	if(numParsedInt64 != 64) {
		fatalError("String to Numeric Conversion Tests Failed with numParsedInt64")
	} //end if
	if(int64(numParsedInt) != numParsedInt64) {
		fatalError("Numeric Tests Failed with numParsedInt vs. numParsedInt64")
	} //end if
	if(uint64(numParsedInt) != uint64(numParsedInt64)) {
		fatalError("Numeric Tests Failed with UINT numParsedInt vs. numParsedInt64")
	} //end if

	fmt.Println("OK: Numeric Tests PASSED")

	fmt.Println("========================= JSON DECODE TESTS =========================")

	testJsonDecode()
	time.Sleep(2 * time.Second)

	fmt.Println("========================= DATE/TIME TESTS =========================")

	fmt.Println("---------- Date Now() ----------")
	testDTimeNow()
	fmt.Println("---------- Date Parse ----------")
	testDTimeParse()

	fmt.Println("========================= TRIM TESTS =========================")

	testAllTrimWhitespaces(input)

	fmt.Println("========================= BASE CONVERT TESTS =========================")

	var baseUnicodeTestStr string = "0" + unicodeString
	fmt.Println("Unicode String:", baseUnicodeTestStr)

	strB32Enc := b32.Encode([]byte(baseUnicodeTestStr)) // 000Unicode ... text
	fmt.Println("Base32-Enc:", strB32Enc)
	strB32Dec, errB32Dec := b32.Decode(strB32Enc)
	if(errB32Dec != nil) {
		fatalError("ERROR: B32 Decode Test Failed with Errors:", errB32Dec)
	}
	if(string(strB32Dec) != baseUnicodeTestStr) {
		fatalError("ERROR: B32 Decode String does not match `" + string(strB32Dec) + "` :: `" + baseUnicodeTestStr + "`")
	}

	strB36Enc := b36.Encode([]byte(baseUnicodeTestStr)) // 0Unicode ... text
	fmt.Println("Base36-Enc:", strB36Enc)
	strB36Dec, errB36Dec := b36.Decode(strB36Enc)
	if(errB36Dec != nil) {
		fatalError("ERROR: B36 Decode Test Failed with Errors:", errB36Dec)
	}
	if(string(strB36Dec) != baseUnicodeTestStr) {
		fatalError("ERROR: B36 Decode String does not match `" + string(strB36Dec) + "` :: `" + baseUnicodeTestStr + "`")
	}

	strB58Enc := b58.Encode([]byte(baseUnicodeTestStr))
	fmt.Println("Base58-Enc:", strB58Enc)
	strB58Dec, errB58Dec := b58.Decode(strB58Enc)
	if(errB58Dec != nil) {
		fatalError("ERROR: B58 Decode Test Failed with Errors:", errB58Dec)
	}
	if(string(strB58Dec) != baseUnicodeTestStr) {
		fatalError("ERROR: B58 Decode String does not match `" + string(strB58Dec) + "` :: `" + baseUnicodeTestStr + "`")
	}

	strB62Enc := b62.Encode([]byte(baseUnicodeTestStr)) // 0Unicode ... text
	fmt.Println("Base62-Enc:", strB62Enc)
	strB62Dec, errB62Dec := b62.Decode(strB62Enc)
	if(errB62Dec != nil) {
		fatalError("ERROR: B62 Decode Test Failed with Errors:", errB62Dec)
	}
	if(string(strB62Dec) != baseUnicodeTestStr) {
		fatalError("ERROR: B62 Decode String does not match `" + string(strB62Dec) + "` :: `" + baseUnicodeTestStr + "`")
	}

	strB64Enc := smart.Base64sEncode(baseUnicodeTestStr)
	fmt.Println("Base64s-Enc:", strB64Enc)
	strB64Dec := smart.Base64sDecode(strB64Enc)
	if(string(strB64Dec) != baseUnicodeTestStr) {
		fatalError("ERROR: B64 Decode String does not match `" + string(strB64Dec) + "` :: `" + baseUnicodeTestStr + "`")
	}

	strB85Enc := b85.Encode([]byte(baseUnicodeTestStr)) // 0Unicode ... text
	fmt.Println("Base85-Enc:", strB85Enc)
	strB85Dec, errB85Dec := b85.Decode(strB85Enc)
	if(errB85Dec != nil) {
		fatalError("ERROR: B85 Decode Test Failed with Errors:", errB85Dec)
	}
	if(string(strB85Dec) != baseUnicodeTestStr) {
		fatalError("ERROR: B85 Decode String does not match `" + string(strB85Dec) + "` :: `" + baseUnicodeTestStr + "`")
	}

	strB92Enc := b92.Encode([]byte(baseUnicodeTestStr)) // 00Unicode ... text
	fmt.Println("Base92-Enc:", strB92Enc)
	strB92Dec, errB92Dec := b92.Decode(strB92Enc)
	if(errB92Dec != nil) {
		fatalError("ERROR: B92 Decode Test Failed with Errors:", errB92Dec)
	}
	if(string(strB92Dec) != baseUnicodeTestStr) {
		fatalError("ERROR: B92 Decode String does not match `" + string(strB92Dec) + "` :: `" + baseUnicodeTestStr + "`")
	}

	fmt.Println("========================= HASH TESTS =========================")

	fmt.Println("CRC32B Hex:", smart.Crc32b(unicodeString))
	fmt.Println("CRC32B B36:", smart.Crc32bB36(unicodeString))

	fmt.Println("MD5 Hex:", smart.Md5(unicodeString))
	fmt.Println("MD5 B64:", smart.Md5B64(unicodeString))

	fmt.Println("SHA1 Hex:", smart.Sha1(unicodeString))
	fmt.Println("SHA1 B64:", smart.Sha1B64(unicodeString))

	fmt.Println("SHA256 Hex:", smart.Sha256(unicodeString))
	fmt.Println("SHA256 B64:", smart.Sha256B64(unicodeString))

	fmt.Println("SHA384 Hex:", smart.Sha384(unicodeString))
	fmt.Println("SHA384 B64:", smart.Sha384B64(unicodeString))

	fmt.Println("SHA512 Hex:", smart.Sha512(unicodeString))
	fmt.Println("SHA512 B64:", smart.Sha512B64(unicodeString))

	fmt.Println("========================= BASE64 TESTS =========================")

	b64 := smart.Base64Encode(input)
	fmt.Println("B64-Enc:", b64)
	fmt.Println("B64-Dec:", smart.Base64Decode(b64))

	fmt.Println("========================= BIN/HEX TESTS =========================")

	hex := smart.Bin2Hex(input)
	fmt.Println("HEX-Enc:", hex)
	fmt.Println("HEX-Dec:", smart.Hex2Bin(hex))

	fmt.Println("========================= BLOWFISH.CBC TESTS =========================")

	//--
	var bfKey string = smart.Hex2Bin("54657374556e6974202f2f205468697320697320612074657374206b657920666f722043727970746f20436970686572202e2e2e2124707269766174652d6b65792330393837363534333231233136323931373335353023313632393137333535302e3538373a3a556e69636f646520537472696e67205b2031363239313733353530205d3a204020536d61727420e382b9e3839ee383bce38388202f2f20436c6f7564204170706c69636174696f6e20506c6174666f726d20e382afe383a9e382a6e38389e382a2e38397e383aae382b1e383bce382b7e383a7e383b3e38397e383a9e38383e38388e38395e382a9e383bce383a02027c3a1c3a2c3a3c3a4c3a5c481c483c485c381c382c383c384c385c480c482c484c487c489c48dc3a7c486c488c48cc387c48fc48ec3a8c3a9c3aac3abc493c495c497c49bc499c388c389c38ac38bc492c494c496c49ac498c49dc4a3c49cc4a2c4a5c4a7c4a4c4a6c3acc3adc3aec3afc4a9c4abc4adc889c88bc4afc38cc38dc38ec38fc4a8c4aac4acc888c88ac4aec4b3c4b5c4b2c4b4c4b7c4b6c4bac4bcc4bec582c4b9c4bbc4bdc581c3b1c584c586c588c391c583c585c587c3b2c3b3c3b4c3b5c3b6c58dc58fc591c3b8c593c392c393c394c395c396c58cc58ec590c398c592c595c597c599c594c596c598c899c59fc5a1c59bc59dc39fc898c59ec5a0c59ac59cc89bc5a3c5a5c89ac5a2c5a4c3b9c3bac3bbc3bcc5a9c5abc5adc5afc5b1c5b3c399c39ac39bc39cc5a8c5aac5acc5aec5b0c5b2c5b5c5b4e1ba8fe1bbb3c5b7c3bfc3bde1ba8ee1bbb2c5b6c5b8c39dc5bac5bcc5bec5b9c5bbc5bd2022203c703e3c2f703e0a09093f2026202a205e2024204020212060207e2025202829205b5d207b7d207c205c202f202b202d205f203a203b202c202e202327302e3538363938343030203136323931373335353023")
	//fmt.Println("Bf Key: `" + bfKey + "`")
	//--
	bfInput := input + " " + smart.DateNowUtc()
	fmt.Println("Data-To-Encrypt: `" + bfInput + "`")
	timerStart = time.Now()
	bfData := smart.BlowfishEncryptCBC(bfInput, bfKey)
	durationBFishEnc := time.Since(timerStart)
	fmt.Println("Data-Encrypted: `" + bfData + "`", durationBFishEnc)
	timerStart = time.Now()
	testDecBfData := smart.BlowfishDecryptCBC(bfData, bfKey)
	durationBFishDec := time.Since(timerStart)
	fmt.Println("Data-Decrypted: `" + testDecBfData + "`", durationBFishDec)
	if((testDecBfData != bfInput) || (smart.Sha1(testDecBfData) != smart.Sha1(bfInput))) {
		fatalError("ERROR: BlowfishEncryptCBC TEST Failed ... Decrypted Data is NOT EQUAL with Plain Data")
	} //end if
	//--
	testPhpBfData := `bf448.v2!dpuWjx5-Jq4dcYwUfp3ObqfT6U6_9cbh8Ou3yugC7vbvcfFdobvTtzAQiyUDnG2TzpnJVDC1O45CdhnpmkoDIwGl4YnGqa_T11zdv0zZtSZKuBeE50BP8n5LCbzfWwQ856q1UgS8SOcFgc38sWIM7I0RbP1O7qTbPmPp3-bwHi0.`
	if(smart.BlowfishEncryptCBC(unicodeString, bfKey) != testPhpBfData) {
		fatalError("ERROR: BlowfishEncryptCBC TEST Failed ... Encrypted Data is NOT EQUAL with Encrypted Data from PHP", smart.BlowfishEncryptCBC(unicodeString, bfKey))
	} //end if
	fmt.Println("Data-Encrypted-PHP: `" + testPhpBfData + "`")
	testDecPhpBfData := smart.BlowfishDecryptCBC(testPhpBfData, bfKey)
	fmt.Println("Data-Decrypted-PHP: `" + testDecPhpBfData + "`")
	if((testDecPhpBfData != unicodeString) || (smart.Sha1(testDecPhpBfData) != smart.Sha1(unicodeString))) {
		fatalError("ERROR: BlowfishDecryptCBC TEST Failed ... Decrypted Data is NOT EQUAL with Decrypted Data from PHP")
	} //end if
	//--
	bfV1Key := "some.BlowFish! - Key@Test 2ks i782s982 s2hwgsjh2wsvng2wfs2w78s528 srt&^ # *&^&#*# e3hsfejwsfjh"
	testPhpBfV1Data := `695C491EF3E92DD8975423A91460F05F9DBBFDBE91DC55AE1D96CC43747B096D64CE08F42885D792505A56DF40CEE6B51FC399A3D756FADB4CE9A492BAE157B4B0DB0C6197D0E35B4C69F99266965686CB41628B75EA56CE006518F408CC0AF1`
	testPhpBfV1XData := "bf384.v1!" + testPhpBfV1Data
	blowFishV1Decrypt := smart.BlowfishDecryptCBC(testPhpBfV1Data, bfV1Key)
	if(blowFishV1Decrypt != input) {
		fatalError("ERROR: BlowfishDecryptCBC v1 TEST Failed ...")
	} //end if
	fmt.Println("Data-Decrypted-V1: `" + blowFishV1Decrypt + "`")
	blowFishV1XDecrypt := smart.BlowfishDecryptCBC(testPhpBfV1XData, bfV1Key)
	if(blowFishV1XDecrypt != input) {
		fatalError("ERROR: BlowfishDecryptCBC v1x TEST Failed ...")
	} //end if
	fmt.Println("Data-Decrypted-V1x: `" + blowFishV1XDecrypt + "`")
	//--

	fmt.Println("========================= ULTRA CRYPTO TESTS =========================")

	timerStart = time.Now()
	argon2idPass := smart.SafePassHashArgon2id824(unicodeString)
	durationSafePass := time.Since(timerStart)
	if(argon2idPass != "2#8bkYl7Y1UgK1>wI.<lkVU2gcR+u!-MHNbGW!,z4uzJ&e<%mDnt9ph%okSP!5~[mSEW/cuWDfg`%Y87*X[:UCXe/Uk2*$4$~{k7SU>=hJ6Z-Don2z.^yN!|R.Q^g75'") {
		fatalError("ERROR: Argon2id SafePass Hash have a wrong value:", argon2idPass)
	} //end if
	log.Println("[INFO] Argon2id SafePass Hash (below), Base92 Encoded, (128 Bytes) from raw Argon2id hash (103 Bytes = 824 bits), time=", durationSafePass)
	log.Println("[DATA] Argon2id SafePass Hash is:", argon2idPass)

	threeFishSecret := "This is a secret ..."
	var threeFishEnc string = ""
	var threeFishDec string = ""

	timerStart = time.Now()
	threeFishEnc = smart.ThreefishEncryptCBC(unicodeString, threeFishSecret, true)
	durationThreeFishEncA := time.Since(timerStart)
	timerStart = time.Now()
	threeFishDec = smart.ThreefishDecryptCBC(threeFishEnc, threeFishSecret, true)
	durationThreeFishDecA := time.Since(timerStart)
	fmt.Println("Threefish Encrypted (ARGON2ID):", threeFishEnc)
	if(threeFishDec == unicodeString) {
		log.Println("[OK] Threefish ARGON2ID Encrypt/Decrypt Test Passed:", durationThreeFishEncA, durationThreeFishDecA)
	} else {
		log.Println("[ERROR] Threefish ARGON2ID Encrypt/Decrypt Test FAILED: `" + threeFishDec + "`")
		return
	} //end if else

	timerStart = time.Now()
	threeFishEnc = smart.ThreefishEncryptCBC(unicodeString, threeFishSecret, false)
	durationThreeFishEncD := time.Since(timerStart)
	timerStart = time.Now()
	threeFishDec = smart.ThreefishDecryptCBC(threeFishEnc, threeFishSecret, false)
	durationThreeFishDecD := time.Since(timerStart)
	fmt.Println("Threefish Encrypted (DEFAULT):", threeFishEnc)
	if(threeFishDec == unicodeString) {
		log.Println("[OK] Threefish DEFAULT Encrypt/Decrypt Test Passed:", durationThreeFishEncD, durationThreeFishDecD)
	} else {
		log.Println("[ERROR] Threefish DEFAULT Encrypt/Decrypt Test FAILED: `" + threeFishDec + "`")
		return
	} //end if else

	timerStart = time.Now()
	var largeTFishEncPak string = smart.ThreefishEncryptCBC(strings.Repeat(unicodeString, 100), threeFishSecret, false)
	durationThreeFishEncLarge := time.Since(timerStart)
	log.Println("[DATA] Threefish large packet:", durationThreeFishEncLarge, largeTFishEncPak)
	timerStart = time.Now()
	if(smart.ThreefishDecryptCBC(largeTFishEncPak, threeFishSecret, false) != strings.Repeat(unicodeString, 100)) {
		log.Println("[ERROR] Threefish Large Packet Decrypt Test FAILED !")
		return
	} //end if
	durationThreeFishDecLarge := time.Since(timerStart)
	log.Println("[OK] Threefish Large Packet Decrypt:", durationThreeFishDecLarge)

	time.Sleep(5 * time.Second)

	fmt.Println("========================= DATA ARCH/UNARCH TESTS =========================")

	// INFO: enc data difers a little from PHP, maybe by some zlib metadata, but decode must work
	var phpStrGzEncodedB64 string = "H4sIAAAAAAAAAwvJyCxWAKJEheKSosy8dAA/Y3YIEAAAAA=="
	var phpStrOriginalGzEnc = "This is a string"
	phpStrGzDecodedB64 := smart.GzDecode(smart.Base64Decode(phpStrGzEncodedB64))
	fmt.Println("GzDecode", phpStrGzDecodedB64);
	if(phpStrGzDecodedB64 != phpStrOriginalGzEnc) {
		fatalError("ERROR: Data GzEnc/GzDec TEST Failed ... Unarchived Data is NOT EQUAL with Unarchived Data from PHP")
	} //end if
	goStrGzEncodedB64 := smart.Base64Encode(smart.GzEncode(phpStrOriginalGzEnc, 9))
	if(smart.GzDecode(smart.Base64Decode(goStrGzEncodedB64)) != phpStrOriginalGzEnc) {
		fatalError("ERROR: Data GzEnc/GzDec TEST Failed ... Archive + Unarchive Data is NOT EQUAL with Unarchived Data from PHP")
	} //end if
	fmt.Println("GzEncode", goStrGzEncodedB64);

	arch := smart.DataArchive(input)
	if(arch == "") {
		fatalError("ERROR: Data Arch is Empty")
	} //end if
	fmt.Println("Data-Arch: `" + arch + "`")
	fmt.Println("Data-UnArch: `" + smart.DataUnArchive(arch) + "`")

	// INFO: arch data difers a little from PHP, maybe by some zlib metadata, but decrypt must work
	testPhpArchData := `HclBDkBAEETRw1hLplupZimDSMRKHMD06Psfgdj9/IfM1ZQ9Z00YLVlnfxNc+Zt+j6Phc+HM3tDkbcn7eR3tuU3SDKGhjwrCUaM4i6dbS7r9qRgEdIsq6i8=` + "\n" + `PHP.SF.151129/B64.ZLibRaw.HEX`;
	fmt.Println("Data-Arch-PHP: `" + testPhpArchData + "`")
	testPhpUnArchData := smart.DataUnArchive(testPhpArchData)
	fmt.Println("Data-Arch-PHP-v2: `" + smart.DataArchive(testPhpUnArchData) + "`")
	fmt.Println("Data-UnArch-PHP: `" + testPhpUnArchData + "`")
	if(testPhpUnArchData != input) {
		fatalError("ERROR: Data Archive/Unarchive TEST Failed ... Data from Archive is NOT EQUAL with Data from PHP Archive")
	} //end if

	fmt.Println("========================= STRING TESTS =========================")

//	var test2Str string = " Lorem Ipsum șȘțȚâÂăĂîÎ is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.       \r\n      \r\n\r\n"

	testStrings()

	fmt.Println("========================= UUID TESTS =========================")

	var u uint64 = uid.UuidSessionSequence()
	u = uid.UuidSessionSequence()
	u = uid.UuidSessionSequence()
	if(u != 3) {
		fatalError("ERROR: UuidSessionSequence FAILED. Expected Result is 3 but get:", u)
	} //end if
	fmt.Println("Session Incremental UUID:", u)

	var uid10 = uid.Uuid1013Str(10)
	if((len(uid10) != 10) || (len(smart.StrTrimWhitespaces(uid10)) != 10)) {
		fatalError("ERROR: UuidStr(10) FAILED. Expected Result is a string of 10 characters length but get a string of :", len(uid10), "characters length, as: `" + uid10 + "`")
	} //end if
	fmt.Println("UUID-10:", uid10)

	var uid13 = uid.Uuid1013Str(13)
	if((len(uid13) != 13) || (len(smart.StrTrimWhitespaces(uid13)) != 13)) {
		fatalError("ERROR: UuidStr(13) FAILED. Expected Result is a string of 13 characters length but get a string of :", len(uid13), "characters length, as: `" + uid13 + "`")
	} //end if
	fmt.Println("UUID-13:", uid13)

	fmt.Println("========================= Json Encode TEST =========================")

	var theStrToEncAsJson string = "\n<c d=\"About:https://a.b?d=1&c=2\">aA-șȘțȚâÂăĂîÎ_+100.12345678901"
	fmt.Println("Json Encoded: `" + smart.JsonEncode(theStrToEncAsJson) + "` from `" + theStrToEncAsJson + "`")

	fmt.Println("========================= MarkerTPL TESTS =========================")

	var arr = map[string]string{
		"TEST1": "one",
		"TEST2": "two",
		"TEST3": "",
		"MARKER": 	THE_JSON,
		"MARKER2": 	`<Tyler="test"` + "\n" + `>`,
		"MARKER3": 	`this_is_an_ID` + ` Test for AddCSlashes: ` + smart.AddCSlashes(`single and double quotes '"`, `'"`),
		"MARKER4": "1.2",
		"MARKER5": "0.0001",
		"NUM-ONE": "100.1",
		"NUM-TWO": "100.2",
	}

	timerStart = time.Now()
	tpl := smart.MarkersTplRender(THE_TPL, arr, false, false, false, false)
	durationMtplRender := time.Since(timerStart)
	time.Sleep(2 * time.Second)
	fmt.Println("\n" + "Raw TPL: `" + THE_TPL + "`" + "\n")
	time.Sleep(1 * time.Second)
	eTpl := smart.MarkersTplEscapeTpl(THE_TPL)
	fmt.Println("\n" + "Escaped TPL (for javascript): `" + eTpl + "`" + "\n")
	fmt.Println("\n" + "Escaped TPL (for javascript) + RawUrlDecode: `" + smart.RawUrlDecode(eTpl) + "`" + "\n")
	fmt.Println("---------- ---------- ----------")
	fmt.Println("\n" + "Rendered TPL: `" + tpl + "`" + "\n")
	log.Println("[OK] MTPL Render:", durationMtplRender, " " + "\n\n")
	time.Sleep(2 * time.Second)

	fmt.Println("========================= File System TESTS =========================")

	testFileSystem(input)

	fmt.Println("========================= Net Validation TESTS =========================")

	testNetFx()

	fmt.Println("========================= Run Timed Cmd TEST =========================")

	testExecTimedProgr()

	fmt.Println("========================= Run Cmd TEST =========================")

	testExecProgr()

	log.Println("[OK] ========================= # ALL TESTS DONE # ...")

} //END FUNCTION


// #END
