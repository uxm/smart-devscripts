
// GO Lang :: Smart HTTP Client
// r.20220421.1924 :: STABLE

package main

import (
	"log"

	smart 			"github.com/unix-world/smartgo"
	smarthttputils 	"github.com/unix-world/smartgo/web-httputils"
)

const (
	cookieUUIDName string = "SmartFramework__UUID"
	cookieUUIDVal string = "P8yZ8T22fQoaTeijFBl2oUD7LpekZS31h0WLjcZ8CmLMrTH5jdDk8kjnu4UGDzAGFYKAh1ZFCjfl247TE4yKTA"

	smartFrameworkUriPrefix string = "https://127.0.0.1/sites/smart-framework/"
	smartFrameworkCloudSuffix string = "admin.php/page/cloud.files/~"
	authUsername string = "admin"
	authPassword string = "the-pass"

	downloadLocalDirPath string = "dwn/"
	testTextFile string = "test.txt"
)

func LogToConsoleWithColors() {
	//--
	smart.ClearPrintTerminal()
	//--
//	smart.LogToStdErr("DEBUG")
	smart.LogToConsole("DEBUG", true) // colored or not
//	smart.LogToFile("WARNING", "logs/", "json", true, true) // json | plain ; also on console ; colored or not
	//--
//	log.Println("[DATA] Data Debugging")
//	log.Println("[DEBUG] Debugging")
//	log.Println("[NOTICE] Notice")
//	log.Println("[WARNING] Warning")
//	log.Println("[ERROR] Error")
//	log.Println("[OK] OK")
//	log.Println("A log message, with no type") // aka [INFO]
	//--
} //END FUNCTION


func main() {

	LogToConsoleWithColors()

	var httpResult *smarthttputils.HttpClientRequest
	var method string = ""
	var uri string = ""
	var ckyArr map[string]string = map[string]string{}
	var reqArr map[string][]string = map[string][]string{}


	if(!smart.PathExists(downloadLocalDirPath)) {
		smart.SafePathDirCreate(downloadLocalDirPath, true, false)
	} //end if
	if(!smart.PathIsDir(downloadLocalDirPath)) {
		log.Println("[ERROR] The local Downloads Dir cannot be created: `" + downloadLocalDirPath + "`")
		return
	} //end if
	smart.SafePathFileWrite(downloadLocalDirPath + testTextFile, "w", false, "abc\ndef")
	if(!smart.PathIsFile(downloadLocalDirPath + testTextFile)) {
		log.Println("[ERROR] The local Test Text File cannot be created: `" + downloadLocalDirPath + testTextFile + "`")
		return
	} //end if


	log.Println("******* Test HEAD *******")
	uri = smartFrameworkUriPrefix + "?page=samples.test-image"
	ckyArr = map[string]string{
		cookieUUIDName: cookieUUIDVal,
	}
	httpResult = smarthttputils.HttpClientDoRequestHEAD(uri, "", true, ckyArr, 0, smarthttputils.HTTP_CLI_MAX_REDIRECTS, "", "")
	httpResult.BodyData = ""
	if(httpResult.HttpStatus != 200) {
		log.Println("[DATA]", "httpResult :: HEAD", smart.JsonRawEncodePretty(httpResult), "\n\n")
		log.Println("[ERROR] HEAD", httpResult.HttpStatus)
		return
	} //end if
	log.Println("[OK] HEAD")


	log.Println("******* Test GET *******")
	uri = smartFrameworkUriPrefix + "?page=samples.test-image"
//	uri = "https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-11.3.0-amd64-netinst.iso"
	ckyArr = map[string]string{
		cookieUUIDName: cookieUUIDVal,
	}
	httpResult = smarthttputils.HttpClientDoRequestGET(uri, "", true, ckyArr, 0, smarthttputils.HTTP_CLI_MAX_BODY_READ_SIZE, 0, "", "")
	httpResult.BodyData = ""
	if(httpResult.HttpStatus != 200) {
		log.Println("[DATA]", "httpResult :: GET", smart.JsonRawEncodePretty(httpResult), "\n\n")
		log.Println("[ERROR] GET", httpResult.HttpStatus)
		return
	} //end if
	log.Println("[OK] GET")


	log.Println("******* Test GET :: DownloadFile *******")
	uri = smartFrameworkUriPrefix + "?page=samples.test-image"
//	uri = "https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-11.3.0-amd64-netinst.iso"
//	uri = "https://cdimage.debian.org/debian-cd/current/amd64/iso-dvd/debian-11.3.0-amd64-DVD-1.iso"
//	uri = "http://www.tinycorelinux.net/13.x/x86/release/CorePlus-current.iso"
	method = "GET"
	ckyArr = nil
	reqArr = nil
	httpResult = smarthttputils.HttpClientDoRequestDownloadFile(downloadLocalDirPath, method, uri, "", true, ckyArr, reqArr, 0, smarthttputils.HTTP_CLI_MAX_REDIRECTS, "", "")
	httpResult.BodyData = ""
	if(httpResult.HttpStatus != 200) {
		log.Println("[DATA]", "httpResult :: GET :: DownloadFile", smart.JsonRawEncodePretty(httpResult), "\n\n")
		log.Println("[ERROR] GET :: DownloadFile", httpResult.HttpStatus)
		return
	} //end if
	log.Println("[OK] GET :: DownloadFile")


	log.Println("******* Test POST :: DownloadFile *******")
	uri = smartFrameworkUriPrefix
	ckyArr = map[string]string{
		cookieUUIDName: cookieUUIDVal,
	}
	reqArr = map[string][]string{
		"page": { "test-image" },
	}
	method = "POST"
	httpResult = smarthttputils.HttpClientDoRequestDownloadFile(downloadLocalDirPath, method, uri, "", true, ckyArr, reqArr, 0, smarthttputils.HTTP_CLI_MAX_REDIRECTS, "", "")
	httpResult.BodyData = ""
	if(httpResult.HttpStatus != 200) {
		log.Println("[DATA]", "httpResult :: POST :: DownloadFile", smart.JsonRawEncodePretty(httpResult), "\n\n")
		log.Println("[ERROR] POST :: DownloadFile", httpResult.HttpStatus)
		return
	} //end if
	log.Println("[OK] POST :: DownloadFile")


	log.Println("******* Test POST *******")
	uri = smartFrameworkUriPrefix
	ckyArr = map[string]string{
		cookieUUIDName: cookieUUIDVal,
	}
	reqArr = map[string][]string{
		"page": { "test-image" },
	}
	httpResult = smarthttputils.HttpClientDoRequestPOST(uri, "", true, ckyArr, reqArr, 0, smarthttputils.HTTP_CLI_MAX_BODY_READ_SIZE, smarthttputils.HTTP_CLI_MAX_REDIRECTS, "", "")
	httpResult.BodyData = ""
	if(httpResult.HttpStatus != 200) {
		log.Println("[DATA]", "httpResult :: POST:", smart.JsonRawEncodePretty(httpResult), "\n\n")
		log.Println("[ERROR] POST", httpResult.HttpStatus)
		return
	} //end if
	log.Println("[OK] POST :: WebDAV")


	log.Println("******* Test DELETE :: WebDAV / Folder *******")
	ckyArr = nil
	uri = smartFrameworkUriPrefix + smartFrameworkCloudSuffix + "/uploads"
	httpResult = smarthttputils.HttpClientDoRequestDELETE(uri, "", true, ckyArr, 0, smarthttputils.HTTP_CLI_MAX_REDIRECTS, authUsername, authPassword)
	httpResult.BodyData = ""
	if(httpResult.HttpStatus != 204) {
		log.Println("[DATA]", "httpResult :: DELETE WebDAV / Folder:", smart.JsonRawEncodePretty(httpResult), "\n\n")
		log.Println("[ERROR] DELETE :: WebDAV / Folder", httpResult.HttpStatus)
		return
	} //end if
	log.Println("[OK] DELETE :: WebDAV / Folder")


	log.Println("******* Test MKCOL :: WebDAV / Folder *******")
	ckyArr = nil
	uri = smartFrameworkUriPrefix + smartFrameworkCloudSuffix + "/uploads"
	httpResult = smarthttputils.HttpClientDoRequestMKCOL(uri, "", true, ckyArr, 0, smarthttputils.HTTP_CLI_MAX_REDIRECTS, authUsername, authPassword)
	httpResult.BodyData = ""
	if(httpResult.HttpStatus != 201) {
		log.Println("[DATA]", "httpResult :: MKCOL WebDAV / Folder:", smart.JsonRawEncodePretty(httpResult), "\n\n")
		log.Println("[ERROR] MKCOL :: WebDAV / Folder", httpResult.HttpStatus)
		return
	} //end if
	log.Println("[OK] MKCOL :: WebDAV / Folder")


	log.Println("******* Test MKCOL :: WebDAV / Sub-Folder *******")
	ckyArr = nil
	uri = smartFrameworkUriPrefix + smartFrameworkCloudSuffix + "/uploads/a"
	httpResult = smarthttputils.HttpClientDoRequestMKCOL(uri, "", true, ckyArr, 0, smarthttputils.HTTP_CLI_MAX_REDIRECTS, authUsername, authPassword)
	httpResult.BodyData = ""
	if(httpResult.HttpStatus != 201) {
		log.Println("[DATA]", "httpResult :: MKCOL WebDAV / Sub-Folder:", smart.JsonRawEncodePretty(httpResult), "\n\n")
		log.Println("[ERROR] MKCOL :: WebDAV / Sub-Folder", httpResult.HttpStatus)
		return
	} //end if
	log.Println("[OK] MKCOL :: WebDAV / Sub-Folder")


	log.Println("******* Test MKCOL :: WebDAV / Sub-Folder-Alt *******")
	ckyArr = nil
	uri = smartFrameworkUriPrefix + smartFrameworkCloudSuffix + "/uploads/b"
	httpResult = smarthttputils.HttpClientDoRequestMKCOL(uri, "", true, ckyArr, 0, smarthttputils.HTTP_CLI_MAX_REDIRECTS, authUsername, authPassword)
	httpResult.BodyData = ""
	if(httpResult.HttpStatus != 201) {
		log.Println("[DATA]", "httpResult :: MKCOL WebDAV / Sub-Folder-Alt:", smart.JsonRawEncodePretty(httpResult), "\n\n")
		log.Println("[ERROR] MKCOL :: WebDAV / Sub-Folder-Alt", httpResult.HttpStatus)
		return
	} //end if
	log.Println("[OK] MKCOL :: WebDAV / Sub-Folder-Alt")


	log.Println("******* Test MKCOL :: WebDAV / Sub-Folder-Alt-Sub *******")
	ckyArr = nil
	uri = smartFrameworkUriPrefix + smartFrameworkCloudSuffix + "/uploads/b/c"
	httpResult = smarthttputils.HttpClientDoRequestMKCOL(uri, "", true, ckyArr, 0, smarthttputils.HTTP_CLI_MAX_REDIRECTS, authUsername, authPassword)
	httpResult.BodyData = ""
	if(httpResult.HttpStatus != 201) {
		log.Println("[DATA]", "httpResult :: MKCOL WebDAV / Sub-Folder-Alt-Sub:", smart.JsonRawEncodePretty(httpResult), "\n\n")
		log.Println("[ERROR] MKCOL :: WebDAV / Sub-Folder-Alt-Sub", httpResult.HttpStatus)
		return
	} //end if
	log.Println("[OK] MKCOL :: WebDAV / Sub-Folder-Alt-Sub")


	log.Println("******* Test POST :: WebDAV *******")
	uri = smartFrameworkUriPrefix + smartFrameworkCloudSuffix + "/uploads"
	ckyArr = map[string]string{
		cookieUUIDName: cookieUUIDVal,
	}
	reqArr = map[string][]string{
		"webdav_action": { "upf" },
		"@file": { downloadLocalDirPath + testTextFile },
	}
	httpResult = smarthttputils.HttpClientDoRequestPOST(uri, "", true, ckyArr, reqArr, 0, smarthttputils.HTTP_CLI_MAX_BODY_READ_SIZE, smarthttputils.HTTP_CLI_MAX_REDIRECTS, authUsername, authPassword)
	httpResult.BodyData = ""
	if(httpResult.HttpStatus != 200) {
		log.Println("[DATA]", "httpResult (POST):", smart.JsonRawEncodePretty(httpResult), "\n\n")
		log.Println("[ERROR] POST", httpResult.HttpStatus)
		return
	} //end if
	log.Println("[OK] POST :: WebDAV")


	log.Println("******* Test DELETE :: WebDAV / File *******")
	ckyArr = nil
	uri = smartFrameworkUriPrefix + smartFrameworkCloudSuffix + "/uploads/" + testTextFile
	httpResult = smarthttputils.HttpClientDoRequestDELETE(uri, "", true, ckyArr, 0, smarthttputils.HTTP_CLI_MAX_REDIRECTS, authUsername, authPassword)
	httpResult.BodyData = ""
	if(httpResult.HttpStatus != 204) {
		log.Println("[DATA]", "httpResult :: DELETE WebDAV / File:", smart.JsonRawEncodePretty(httpResult), "\n\n")
		log.Println("[ERROR] DELETE :: WebDAV / File", httpResult.HttpStatus)
		return
	} //end if
	log.Println("[OK] DELETE :: WebDAV / File")


	log.Println("******* Test PUT :: WebDAV / File *******")
	ckyArr = nil
	var putData string = "abc 123"
	uri = smartFrameworkUriPrefix + smartFrameworkCloudSuffix + "/uploads/" + "other-file.txt"
	httpResult = smarthttputils.HttpClientDoRequestPUT(putData, uri, "", true, ckyArr, 0, smarthttputils.HTTP_CLI_MAX_REDIRECTS, authUsername, authPassword)
	httpResult.BodyData = ""
	if(httpResult.HttpStatus != 201) {
		log.Println("[DATA]", "httpResult :: PUT :: WebDAV / File", smart.JsonRawEncodePretty(httpResult), "\n\n")
		log.Println("[ERROR] PUT :: WebDAV / File", httpResult.HttpStatus)
		return
	} //end if
	log.Println("[OK] PUT :: WebDAV / File")


	log.Println("******* Test PUTFile :: WebDAV / File *******")
	uri = smartFrameworkUriPrefix + smartFrameworkCloudSuffix + "/uploads/"
	ckyArr = nil
	httpResult = smarthttputils.HttpClientDoRequestPUTFile(downloadLocalDirPath + testTextFile, uri, "", true, ckyArr, 0, smarthttputils.HTTP_CLI_MAX_REDIRECTS, authUsername, authPassword)
	httpResult.BodyData = ""
	if(httpResult.HttpStatus != 201) {
		log.Println("[DATA]", "httpResult :: PUTFile :: WebDAV / File", smart.JsonRawEncodePretty(httpResult), "\n\n")
		log.Println("[ERROR] PUTFile :: WebDAV / File", httpResult.HttpStatus)
		return
	} //end if
	log.Println("[OK] PUTFile :: WebDAV / File")


	log.Println("******* Test GET :: WebDAV / File *******")
	uri = smartFrameworkUriPrefix + smartFrameworkCloudSuffix + "/uploads/" + testTextFile
	ckyArr = map[string]string{
		cookieUUIDName: cookieUUIDVal,
	}
	httpResult = smarthttputils.HttpClientDoRequestGET(uri, "", true, ckyArr, 0, smarthttputils.HTTP_CLI_MAX_BODY_READ_SIZE, 0, authUsername, authPassword)
	httpResult.BodyData = ""
	if(httpResult.HttpStatus != 200) {
		log.Println("[DATA]", "httpResult :: GET :: WebDAV / File", smart.JsonRawEncodePretty(httpResult), "\n\n")
		log.Println("[ERROR] GET :: WebDAV / File", httpResult.HttpStatus)
		return
	} //end if
	log.Println("[OK] GET :: WebDAV / File")


	log.Println("******* Test COPY :: WebDAV / File *******")
	uri = smartFrameworkUriPrefix + smartFrameworkCloudSuffix + "/uploads/" + testTextFile
	ckyArr = nil
	httpResult = smarthttputils.HttpClientDoRequestCOPY(smartFrameworkUriPrefix + smartFrameworkCloudSuffix + "/uploads/a/" + testTextFile, true, uri, "", true, ckyArr, 0, smarthttputils.HTTP_CLI_MAX_REDIRECTS, authUsername, authPassword)
	httpResult.BodyData = ""
	if(httpResult.HttpStatus != 201) {
		log.Println("[DATA]", "httpResult :: COPY WebDAV / File:", smart.JsonRawEncodePretty(httpResult), "\n\n")
		log.Println("[ERROR] COPY :: WebDAV / File", httpResult.HttpStatus)
		return
	} //end if
	log.Println("[OK] COPY :: WebDAV / File")


	log.Println("******* Test MOVE :: WebDAV / File *******")
	uri = smartFrameworkUriPrefix + smartFrameworkCloudSuffix + "/uploads/" + testTextFile
	ckyArr = nil
	httpResult = smarthttputils.HttpClientDoRequestMOVE(smartFrameworkUriPrefix + smartFrameworkCloudSuffix + "/uploads/b/" + testTextFile, true, uri, "", true, ckyArr, 0, smarthttputils.HTTP_CLI_MAX_REDIRECTS, authUsername, authPassword)
	httpResult.BodyData = ""
	if(httpResult.HttpStatus != 201) {
		log.Println("[DATA]", "httpResult :: MOVE WebDAV / File:", smart.JsonRawEncodePretty(httpResult), "\n\n")
		log.Println("[ERROR] MOVE :: WebDAV / File", httpResult.HttpStatus)
		return
	} //end if
	log.Println("[OK] MOVE :: WebDAV / File")


	log.Println("******* Test COPY :: WebDAV / Folder *******")
	uri = smartFrameworkUriPrefix + smartFrameworkCloudSuffix + "/uploads/a/"
	ckyArr = nil
	httpResult = smarthttputils.HttpClientDoRequestCOPY(smartFrameworkUriPrefix + smartFrameworkCloudSuffix + "/uploads/b/c/", true, uri, "", true, ckyArr, 0, smarthttputils.HTTP_CLI_MAX_REDIRECTS, authUsername, authPassword)
	httpResult.BodyData = ""
	if(httpResult.HttpStatus != 204) {
		log.Println("[DATA]", "httpResult :: COPY WebDAV / Folder:", smart.JsonRawEncodePretty(httpResult), "\n\n")
		log.Println("[ERROR] COPY :: WebDAV / Folder", httpResult.HttpStatus)
		return
	} //end if
	log.Println("[OK] COPY :: WebDAV / Folder")


	log.Println("******* Test MOVE :: WebDAV / Folder *******")
	uri = smartFrameworkUriPrefix + smartFrameworkCloudSuffix + "/uploads/b/c"
	ckyArr = nil
	httpResult = smarthttputils.HttpClientDoRequestMOVE(smartFrameworkUriPrefix + smartFrameworkCloudSuffix + "/uploads/a/c", true, uri, "", true, ckyArr, 0, smarthttputils.HTTP_CLI_MAX_REDIRECTS, authUsername, authPassword)
	httpResult.BodyData = ""
	if(httpResult.HttpStatus != 201) {
		log.Println("[DATA]", "httpResult :: MOVE WebDAV / Folder:", smart.JsonRawEncodePretty(httpResult), "\n\n")
		log.Println("[ERROR] MOVE :: WebDAV / Folder", httpResult.HttpStatus)
		return
	} //end if
	log.Println("[OK] MOVE :: WebDAV / Folder")


	log.Println("******* Test DELETE :: WebDAV / Folder *******")
	ckyArr = nil
	uri = smartFrameworkUriPrefix + smartFrameworkCloudSuffix + "/uploads/a"
	httpResult = smarthttputils.HttpClientDoRequestDELETE(uri, "", true, ckyArr, 0, smarthttputils.HTTP_CLI_MAX_REDIRECTS, authUsername, authPassword)
	httpResult.BodyData = ""
	if(httpResult.HttpStatus != 204) {
		log.Println("[DATA]", "httpResult :: DELETE WebDAV / Folder:", smart.JsonRawEncodePretty(httpResult), "\n\n")
		log.Println("[ERROR] DELETE :: WebDAV / Folder", httpResult.HttpStatus)
		return
	} //end if
	log.Println("[OK] DELETE :: WebDAV / Folder")


	log.Println("******* Test PROPFIND :: WebDAV / N/A File/Folder *******")
	ckyArr = nil
	uri = smartFrameworkUriPrefix + smartFrameworkCloudSuffix + "/uploads/z"
	httpResult = smarthttputils.HttpClientDoRequestPROPFIND(uri, "", true, ckyArr, 0, smarthttputils.HTTP_CLI_MAX_REDIRECTS, authUsername, authPassword)
	httpResult.BodyData = ""
	if(httpResult.HttpStatus != 404) {
		log.Println("[DATA]", "httpResult :: PROPFIND :: WebDAV / N/A File/Folder:", smart.JsonRawEncodePretty(httpResult), "\n\n")
		log.Println("[ERROR] PROPFIND :: WebDAV / N/A File/Folder", httpResult.HttpStatus)
		return
	} //end if
	log.Println("[OK] PROPFIND :: WebDAV / N/A File/Folder")


	log.Println("******* Test PROPFIND :: WebDAV / Folder *******")
	ckyArr = nil
	uri = smartFrameworkUriPrefix + smartFrameworkCloudSuffix + "/uploads/b"
	httpResult = smarthttputils.HttpClientDoRequestPROPFIND(uri, "", true, ckyArr, 0, smarthttputils.HTTP_CLI_MAX_REDIRECTS, authUsername, authPassword)
	httpResult.BodyData = ""
	if(httpResult.HttpStatus != 207) {
		log.Println("[DATA]", "httpResult :: PROPFIND :: WebDAV / Folder:", smart.JsonRawEncodePretty(httpResult), "\n\n")
		log.Println("[ERROR] PROPFIND :: WebDAV / Folder", httpResult.HttpStatus)
		return
	} //end if
	log.Println("[OK] PROPFIND :: WebDAV / Folder")


	log.Println("******* Test PROPFIND :: WebDAV / File *******")
	ckyArr = nil
	uri = smartFrameworkUriPrefix + smartFrameworkCloudSuffix + "/uploads/b/" + testTextFile
	httpResult = smarthttputils.HttpClientDoRequestPROPFIND(uri, "", true, ckyArr, 0, smarthttputils.HTTP_CLI_MAX_REDIRECTS, authUsername, authPassword)
	httpResult.BodyData = ""
	if(httpResult.HttpStatus != 207) {
		log.Println("[DATA]", "httpResult :: PROPFIND :: WebDAV / File:", smart.JsonRawEncodePretty(httpResult), "\n\n")
		log.Println("[ERROR] PROPFIND :: WebDAV / File", httpResult.HttpStatus)
		return
	} //end if
	log.Println("[OK] PROPFIND :: WebDAV / File")


	log.Println("******* Test OPTIONS :: WebDAV *******")
	ckyArr = nil
	uri = smartFrameworkUriPrefix + smartFrameworkCloudSuffix
	httpResult = smarthttputils.HttpClientDoRequestOPTIONS(uri, "", true, ckyArr, 0, smarthttputils.HTTP_CLI_MAX_REDIRECTS, authUsername, authPassword)
	if(httpResult.HttpStatus != 200) {
		log.Println("[DATA]", "httpResult :: OPTIONS :: WebDAV:", smart.JsonRawEncodePretty(httpResult), "\n\n")
		log.Println("[ERROR] OPTIONS :: WebDAV", httpResult.HttpStatus)
		return
	} //end if
	log.Println("[OK] OPTIONS :: WebDAV")


	log.Println("******* Test DELETE :: WebDAV / Folder (Cleanup) *******")
	ckyArr = nil
	uri = smartFrameworkUriPrefix + smartFrameworkCloudSuffix + "/uploads"
	httpResult = smarthttputils.HttpClientDoRequestDELETE(uri, "", true, ckyArr, 0, smarthttputils.HTTP_CLI_MAX_REDIRECTS, authUsername, authPassword)
	httpResult.BodyData = ""
	if(httpResult.HttpStatus != 204) {
		log.Println("[DATA]", "httpResult :: DELETE WebDAV / Folder (Cleanup):", smart.JsonRawEncodePretty(httpResult), "\n\n")
		log.Println("[ERROR] DELETE :: WebDAV / Folder (Cleanup)", httpResult.HttpStatus)
		return
	} //end if
	log.Println("[OK] DELETE :: WebDAV / Folder (Cleanup)")


	log.Println("******* Test GET :: WebDAV / Path *******")
	uri = smartFrameworkUriPrefix + smartFrameworkCloudSuffix + "/uploads"
	ckyArr = map[string]string{
		cookieUUIDName: cookieUUIDVal,
	}
	httpResult = smarthttputils.HttpClientDoRequestGET(uri, "", true, ckyArr, 0, smarthttputils.HTTP_CLI_MAX_BODY_READ_SIZE, 0, authUsername, authPassword)
	httpResult.BodyData = ""
	if(httpResult.HttpStatus != 404) {
		log.Println("[DATA]", "httpResult :: GET :: WebDAV / Path", smart.JsonRawEncodePretty(httpResult), "\n\n")
		log.Println("[ERROR] GET :: WebDAV / Path", httpResult.HttpStatus)
		return
	} //end if
	log.Println("[OK] GET :: WebDAV / Path")


	log.Println("******* Test POST :: Smart.Framework / Welcome *******")
	uri = smartFrameworkUriPrefix
	ckyArr = map[string]string{
		cookieUUIDName: cookieUUIDVal,
	}
	reqArr = map[string][]string{
		"page": { "samples.benchmark" },
	}
	httpResult = smarthttputils.HttpClientDoRequestPOST(uri, "", true, ckyArr, reqArr, 0, smarthttputils.HTTP_CLI_MAX_BODY_READ_SIZE, smarthttputils.HTTP_CLI_MAX_REDIRECTS, "", "")
	httpResult.BodyData = ""
	if(httpResult.HttpStatus != 200) {
		log.Println("[DATA]", "httpResult :: POST :: Smart.Framework / Welcome", smart.JsonRawEncodePretty(httpResult), "\n\n")
		log.Println("[ERROR] POST :: Smart.Framework / Welcome", httpResult.HttpStatus)
		return
	} //end if
	log.Println("[OK] POST :: Smart.Framework / Welcome")


	log.Println("******* Test GET :: Smart.Framework / Welcome *******")
	uri = smartFrameworkUriPrefix
	ckyArr = map[string]string{
		cookieUUIDName: cookieUUIDVal,
	}
	httpResult = smarthttputils.HttpClientDoRequestGET(uri, "", true, ckyArr, 0, smarthttputils.HTTP_CLI_MAX_BODY_READ_SIZE, 0, "", "")
	httpResult.BodyData = ""
	if(httpResult.HttpStatus != 202) {
		log.Println("[DATA]", "httpResult :: GET :: Smart.Framework / Welcome", smart.JsonRawEncodePretty(httpResult), "\n\n")
		log.Println("[ERROR] GET :: Smart.Framework / Welcome", httpResult.HttpStatus)
		return
	} //end if
	log.Println("[OK] GET :: Smart.Framework / Welcome")


	httpResult.BodyData = ""
	log.Println("[DATA]", "httpResult:", smart.JsonRawEncodePretty(httpResult), "\n\n")


	if(smart.PathExists(downloadLocalDirPath)) {
		smart.SafePathDirDelete(downloadLocalDirPath, false)
	} //end if
	if(smart.PathExists(downloadLocalDirPath)) {
		log.Println("[ERROR] The local Downloads Dir cannot be deleted: `" + downloadLocalDirPath + "`")
	} //end if
	log.Println("[OK] ####### SmartHttpCli :: All Tests COMPLETED ... #######")


} //END FUNCTION

// #END
