
// GO Lang
// go build main.go ; on openbsd may need to: CGO_LDFLAGS_ALLOW='-Wl,-z,wxneeded|-Wl,-rpath-link,/usr/X11R6/lib' go build main.go
// (c) 2017-2022 unix-world.org
// version: 2022.04.14
// req. go 1.16 or later

package main

import (
	"bytes"
//	"strings"
	"encoding/json"
	"fmt"
//	"time"
	"io"
	"log"
	"mime"
	"net"
	"net/http"
	"path/filepath"

	"github.com/unix-world/smartgoext/webview"

	"embed"
)
//go:embed assets/*
var assets embed.FS
//--

func assetRead(path string) (contents []byte, err error) {
	return  assets.ReadFile("assets/" + path)
}

func startServer() string {
	ln, err := net.Listen("tcp", "127.0.0.1:0")
	if err != nil {
		log.Fatal(err)
	}
	go func() {
		defer ln.Close()
		http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			path := r.URL.Path
			if len(path) > 0 && path[0] == '/' {
				path = path[1:]
			}
			if path == "" {
				path = "index.html"
			}
			if bs, err := assetRead(path); err != nil {
				w.WriteHeader(http.StatusNotFound)
			} else {
				w.Header().Add("Content-Type", mime.TypeByExtension(filepath.Ext(path)))
				io.Copy(w, bytes.NewBuffer(bs))
			}
		})
		log.Fatal(http.Serve(ln, nil))
	}()
	return "http://" + ln.Addr().String()
}

func handleRPC(w webview.WebView, data string) {
	jdata := struct {
		Cmd string `json:"cmd"`
		Msg string `json:"msg"`
		Val string `json:"val"`
	}{}
	if err := json.Unmarshal([]byte(data), &jdata); err != nil {
		fmt.Println("JSON-RPC ERROR !")
		fmt.Println(err)
		return
	}
	switch jdata.Cmd {
		case "close":
			w.Terminate()
		case "fullscreen":
			w.SetFullscreen(true)
		case "unfullscreen":
			w.SetFullscreen(false)
		case "init":
			// do nothing
		case "test":
			fmt.Println(jdata.Msg + ": #" + jdata.Val)
	}
}

func main() {
	fmt.Println("Starting ...")
	url := startServer()
	fmt.Println("Done.")
	w := webview.New(webview.Settings{
		Width:  960,
		Height: 720,
		Title:  "Charts",
		URL:    url,
		ExternalInvokeCallback: handleRPC,
	})
	defer w.Exit()
	w.Run()
}

// #END
