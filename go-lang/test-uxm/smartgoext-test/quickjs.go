
// GoLang Sample QuickJs v1
// Execute JS Code in Golang using QuickJs
// (c) 2022 unix-world.org
// r.20220411.0430

package main

import (
	"runtime"
	"errors"
	"fmt"
	"log"
	"time"
	"sort"

	smart "github.com/unix-world/smartgo"
	assets "github.com/unix-world/smartgo/web-assets"

	"github.com/unix-world/smartgoext/quickjs"
)

func LogToConsoleWithColors() {
	//--
	smart.ClearPrintTerminal()
	//--
//	smart.LogToStdErr("DEBUG")
	smart.LogToConsole("DEBUG", true) // colored or not
//	smart.LogToFile("DEBUG", "logs/", "json", true, true) // json | plain ; also on console ; colored or not
	//--
//	log.Println("[DEBUG] Debugging")
//	log.Println("[DATA] Data")
//	log.Println("[NOTICE] Notice")
//	log.Println("[WARNING] Warning")
//	log.Println("[ERROR] Error")
//	log.Println("[OK] OK")
//	log.Println("A log message, with no type")
	//--
} //END FUNCTION


func consoleError(ctx *quickjs.Context, this quickjs.Value, args []quickjs.Value) quickjs.Value {

	theArgs := map[string]string{}
	for kk, vv := range args {
		theArgs["arg:" + smart.ConvertIntToStr(kk)] = vv.String()
	}
	jsonArgs := smart.JsonEncode(theArgs)

	time.Sleep(1 * time.Second)
	jsonStruct := smart.JsonObjDecode(jsonArgs)
//	log.Println("ERR: JS Console Error", jsonArgs, jsonStruct)
	if(jsonStruct != nil) {
		keys := make([]string, 0)
		for xx, _ := range jsonStruct {
			keys = append(keys, xx)
		}
		sort.Strings(keys)
		var txt string = ""
		for _, zz := range keys {
			txt += jsonStruct[zz].(string) + " "
		}
	//	if(txt == "This is a log message from a method inside go (this will call a go method) ") {
	//		return ctx.String("[OK] JS Console Error: " + "`" + txt + "`")
	//	} else {
	//		return ctx.String("[WARNING] JS Console Error: " + "`" + txt + "`")
	//	}
	}
//	return ctx.String("[ERROR} JS Console Error: [Empty]")
	return ctx.String(jsonArgs)
}

func consoleOk(ctx *quickjs.Context, this quickjs.Value, args []quickjs.Value) quickjs.Value {

	theArgs := map[string]string{}
	for kk, vv := range args {
		theArgs["arg:" + smart.ConvertIntToStr(kk)] = vv.String()
	}
	jsonArgs := smart.JsonEncode(theArgs)

	return ctx.String(jsonArgs)
}


func evalJsCode(loop int) {

	runtime.LockOSThread()
	defer runtime.UnlockOSThread()

	check := func(err error, result quickjs.Value) (theErr string, theCause string, theStack string) {
		if err != nil {
			var evalErr *quickjs.Error
			var cause string = ""
			var stack string = ""
			if errors.As(err, &evalErr) {
				cause = evalErr.Cause
				stack = evalErr.Stack
			}
			return err.Error(), cause, stack
		}
		if(result.IsException()) {
			log.Println("[WARNING] JS Exception !", result)
			return "", "", ""
		}
		return "", "", ""
	}

/*
	js := `
function fib(n) {
  if (n < 2) return n;
  return fib(n - 2) + fib(n - 1);
}
//console.log(fib(35));
fib(35);
`
*/

	jsSettings, _ 	:= smart.SafePathFileRead("js/settings.js",  false)
	jsMain, _ 		:= smart.SafePathFileRead("js/main.js",  false)

	jsCoreUtils  := assets.ReadWebAsset("lib/js/framework/src/core_utils.js")
	jsDateUtils  := assets.ReadWebAsset("lib/js/framework/src/date_utils.js")
	jsCryptUtils := assets.ReadWebAsset("lib/js/framework/src/crypt_utils.js")
	var js string = "\n" + jsSettings + "\n\n" + jsCoreUtils + "\n\n" + jsDateUtils + "\n\n" + jsCryptUtils + "\n\n" + jsMain + "\n\n" + "main();" + "\n"
//	fmt.Println(js)

	log.Println("[NOTICE] Js Data Read DONE ...")
	time.Sleep(time.Duration(2) * time.Second)

	//--
	timerStart := time.Now()
	//--
//	consoleError := func(ctx *quickjs.Context, this quickjs.Value, args []quickjs.Value) quickjs.Value {
//		log.Println("[ERROR] Error", args)
//		return ctx.Null()
//	}
	//--
	jsvm := quickjs.NewRuntime()
	defer jsvm.Free()
	const MB = 1 << 10 << 10
	//log.Println("[DEBUG]", "1 MB=", MB, "bytes")
	log.Println("[DEBUG] Settings: JsVm Memory Limit to", 128, "MB")
	jsvm.SetMemoryLimit(128 * MB) // 128M
	//--
	context := jsvm.NewContext()
	defer context.Free()
	//--
	globals := context.Globals()
	json := context.String(smart.JsonEncode(map[string]string{"JSON":"GoLangQuickJsVm", "txt":smart.ConvertIntToStr(loop)+"Unicode String:		şŞţŢăĂîÎâÂșȘțȚ (05-09#"}))
	globals.Set("jsonInput", json)
	//--
	sleepTimeMs := func(ctx *quickjs.Context, this quickjs.Value, args []quickjs.Value) quickjs.Value {
		var mseconds uint64 = 0
		var msecnds string = ""
		for _, vv := range args {
			msecnds = vv.String()
			mseconds = smart.ParseStrAsUInt64(msecnds)
		} //end for
		if(mseconds > 1 && mseconds < 3600 * 1000) {
			time.Sleep(time.Duration(mseconds) * time.Millisecond)
		} //end if
		return ctx.String(msecnds)
	} //end function
	//--
	consoleLog := func(ctx *quickjs.Context, this quickjs.Value, args []quickjs.Value) quickjs.Value {
		//--
		theArgs := map[string]string{}
		for kk, vv := range args {
			theArgs["arg:" + smart.ConvertIntToStr(kk)] = vv.String()
		}
		jsonArgs := smart.JsonEncode(theArgs)
		//--
		jsonStruct := smart.JsonObjDecode(jsonArgs)
		if(jsonStruct != nil) {
			var txt string = ""
			keys := make([]string, 0)
			for xx, _ := range jsonStruct {
				keys = append(keys, xx)
			}
			sort.Strings(keys)
			for _, zz := range keys {
				txt += jsonStruct[zz].(string) + " "
			}
			log.Println(txt)
		} //end if
		//--
	//	return ctx.String("")
		return ctx.String(jsonArgs)
	} //end function
	//--
	globals.SetFunction("SmartJsVm_sleepTimeMs", sleepTimeMs)
	globals.SetFunction("SmartJsVm_consoleLog", consoleLog)
	//--
	globals.Set("SmartJsVmX_consoleError", context.Function(consoleError))
	globals.Set("SmartJsVmX_consoleOk", context.Function(consoleOk))
	//--
	result, err := context.Eval(js, quickjs.EVAL_GLOBAL) // quickjs.EVAL_STRICT
	jsvm.ExecutePendingJob() // req. to execute promises: ex: `new Promise(resolve => resolve('testPromise')).then(msg => console.log('******* Promise Solved *******', msg));`
	defer result.Free()
	jsErr, _, _ := check(err, result)
	if(jsErr != "") {
		log.Println("[ERROR] JS-Error:", "`" + jsErr + "`")
		return
	}
	//--
	durationExecJs := time.Since(timerStart)
	//--
	fmt.Println(durationExecJs, "\n", result.String())
	//--

}

func myAsyncFunction(loop int) <-chan int32 {
	r := make(chan int32)
	go func() {
		defer close(r)
		evalJsCode(loop)
		r <- int32(loop)
	}()
	return r
}

func main() {

	LogToConsoleWithColors()

	for i := 0; i < 10; i++ {
		r := <-myAsyncFunction(i)
		fmt.Println(r)
	}

}
