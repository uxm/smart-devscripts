
// GoLang Sample HTTP Mux
// HTTP Handlers (Gorilla)
// (c) 2022 unix-world.org
// r.20220427.1426

package main

import (
	"runtime"
	"os"
	"log"
	"net/http"

	smart 			"github.com/unix-world/smartgo"
	assets 			"github.com/unix-world/smartgo/web-assets"
	smarthttputils 	"github.com/unix-world/smartgo/web-httputils"

	"github.com/unix-world/smartgoext/gorilla/mux"
	"github.com/unix-world/smartgoext/gorilla/handlers"
	"github.com/unix-world/smartgoext/gorilla/sessions"
	"github.com/unix-world/smartgoext/quickjs"
	jsvm "github.com/unix-world/smartgoext/quickjsvm"
)

/* {{{SYNC-JSLOAD:BINARY}}} */
//#cgo CFLAGS: -I./js/lib
//
//#include "core_utils.c"
//#include "date_utils.c"
//#include "crypt_utils.c"
/* */
//import "C"
//import "unsafe"
/*
func coreUtils() []byte {
	return C.GoBytes(unsafe.Pointer(&C.qjsc_core_utils), C.int(C.qjsc_core_utils_size)) // qjsc -c core_utils.js
//	return []byte{}
}
func dateUtils() []byte {
	return C.GoBytes(unsafe.Pointer(&C.qjsc_date_utils), C.int(C.qjsc_date_utils_size)) // qjsc -c date_utils.js
//	return []byte{}
}
func cryptUtils() []byte {
	return C.GoBytes(unsafe.Pointer(&C.qjsc_crypt_utils), C.int(C.qjsc_crypt_utils_size)) // qjsc -c crypt_utils.js
//	return []byte{}
}
*/

const (
	SESSION_KEY string = "1234567890"
	SERVER_ADDR string = "127.0.0.1:8880"
	TIMEOUT_JS_EXEC_SECONDS uint32 = 60 // seconds
)

func LogToConsoleWithColors() {
	//--
	smart.ClearPrintTerminal()
	//--
//	smart.LogToStdErr("DEBUG")
	smart.LogToConsole("DEBUG", true) // colored or not
//	smart.LogToFile("DEBUG", "logs/", "json", true, true) // json | plain ; also on console ; colored or not
	//--
//	log.Println("[DEBUG] Debugging")
//	log.Println("[DATA] Data")
//	log.Println("[NOTICE] Notice")
//	log.Println("[WARNING] Warning")
//	log.Println("[ERROR] Error")
//	log.Println("[OK] OK")
//	log.Println("A log message, with no type")
	//--
} //END FUNCTION


func evalJsCode(txt string, js string) string {

	runtime.LockOSThread()
	defer runtime.UnlockOSThread()

	consoleOk := func(ctx *quickjs.Context, this quickjs.Value, args []quickjs.Value) quickjs.Value {
		theArgs := map[string]string{}
		for kk, vv := range args {
			theArgs["arg:" + smart.ConvertIntToStr(kk)] = vv.String()
		}
		jsonArgs := smart.JsonEncode(theArgs)
		return ctx.String(jsonArgs)
	}

	jsInputData := map[string]string{"txt":txt}
	jsExtendMethods := map[string]interface{}{
		"consoleOk": consoleOk,
	}
	jsBinaryCodePreload := map[string][]byte{
	//-- {{{SYNC-JSLOAD:BINARY}}}
	//	"core_utils.js":  coreUtils(),
	//	"date_utils.js":  dateUtils(),
	//	"crypt_utils.js": cryptUtils(),
	//-- #endsync
	}

	jsEvErr, jsEvRes := jsvm.QuickJsVmRunCode(js, TIMEOUT_JS_EXEC_SECONDS, 128, jsInputData, jsExtendMethods, jsBinaryCodePreload)
	if(jsEvErr != "") {
		log.Println("[ERROR]", jsEvErr)
		return ""
	}

	return jsEvRes

} //END FUNCTION


func main() {

	LogToConsoleWithColors()

	_, srv := smarthttputils.HttpMuxServer(SERVER_ADDR, uint32(TIMEOUT_JS_EXEC_SECONDS + 60), true, "Sample") // force HTTP/1.1
	r := mux.NewRouter()
//	srv.Handler = r
	srv.Handler = handlers.CompressHandler(r) // serve with gzip compression

	// Note: Don't store your key in your source code. Pass it via an
	// environmental variable, or flag (or both), and don't accidentally commit it
	// alongside your code. Ensure your key is sufficiently random - i.e. use Go's
	// crypto/rand or securecookie.GenerateRandomKey(32) and persist the result.
	var sessionStore = sessions.NewCookieStore([]byte(SESSION_KEY))
	sessionStore.Options.Path = "/"
	sessionStore.Options.SameSite = http.SameSiteLaxMode
	sessionStore.Options.MaxAge = 0

	jsMain, _ := smart.SafePathFileRead("js/http-handlers-mux.js", false)
	var js string = "\n" + jsMain + "\n\n" + "main();" + "\n"

	handleSayHello := func(w http.ResponseWriter, r *http.Request) {

		vars := mux.Vars(r)
		evJs := evalJsCode(string(vars["testpath"]), js)

		// Get a session. We're ignoring the error resulted from decoding an existing session: Get() always returns a session, even if empty
		session, _ := sessionStore.Get(r, "go_srv_sess")
		// Set some session values
		session.Values["foo"] = "bar"
		session.Values[42] = 43
		_ = session.Save(r, w)

		w.Header().Set("Content-Type", assets.HTML_CONTENT_HEADER)
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(assets.HtmlStandaloneTemplate("TestPath", "", `<h1>Test Path</h1>` + `<div class="operation_important">` + smart.EscapeHtml(evJs) + `</div>`)))
	}

	handleShowIndex := func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", assets.HTML_CONTENT_HEADER)
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(assets.HtmlStandaloneTemplate("Index", "", `<h1>Index...</h1>` + `<div>` + `<img width="32" height="32" src="data:image/svg+xml,` + smart.EscapeHtml(smart.EscapeUrl(assets.ReadWebAsset("lib/framework/img/loading-spin.svg"))) + `"></div>` + `</div><hr><br><a class="ux-button" href="/test-123">Test Path</a>`)))
	}

	// Only log requests to our admin dashboard to stdout
	r.Handle("/{testpath}", handlers.LoggingHandler(os.Stdout, http.HandlerFunc(handleSayHello)))
	r.HandleFunc("/", handleShowIndex)

	log.Println("[INFO]", "Web Service Test Path: /{testpath}")

	// Wrap our server with our gzip handler to gzip compress all responses.
	log.Println("[NOTICE]", "Starting HTTP Server at Address " + SERVER_ADDR)
	err := srv.ListenAndServe()

//	log.Println("[NOTICE]", "Starting HTTPS Server on Port 8443")
//	err := http.ListenAndServeTLS(":8443", "cert.crt", "cert.key", handlers.CompressHandler(r))

	if(err != nil) {
		log.Fatal("ListenAndServe: ", err)
	} //end if

} //END FUNCTION


// #END
