
// GoLang Sample
// HTTP Handlers (Gorilla)
// (c) 2022 unix-world.org
// r.20220315

package main

import (
	"os"
	"net/http"

	"github.com/unix-world/smartgoext/gorilla/handlers"
)

func handleSayHello(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(`hello world`))
}

func handleShowIndex(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(`Index...`))
}

func main() {
	r := http.NewServeMux()

	// Only log requests to our admin dashboard to stdout
	r.Handle("/hello", handlers.LoggingHandler(os.Stdout, http.HandlerFunc(handleSayHello)))
	r.HandleFunc("/", handleShowIndex)

	// Wrap our server with our gzip handler to gzip compress all responses.
	http.ListenAndServe(":8888", handlers.CompressHandler(r))
}

// #END
