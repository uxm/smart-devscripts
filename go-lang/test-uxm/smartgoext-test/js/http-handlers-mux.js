
// jsonInput is comming from golang

function main() {

//	SmartJsVm_sleepTimeMs(1000); // test sleep 1 sec

	if(typeof(jsonInput) != 'string') {
		throw Error('ERROR: jsonInput expected from GoLang');
		return '';
	}
	var json;
	try {
		json = JSON.parse(jsonInput)
	} catch(err) {
		throw Error('jsonInput Parse ERROR: ' + err);
		return '';
	}
	if(json['JSON'] !== 'GoLangQuickJsVm') {
		throw Error('jsonInput is INVALID: missing json control field ... json:golang');
		return '';
	} //end if

	var theTxt = String(json['txt'] || '');
//	SmartJsVmX_consoleOk(theTxt);

	var rand = String(Math.floor(Math.random() * 100000));
//	rand = rand.padStart(7, '0');

	return String('Js Execution Result: TestPath = `' + theTxt + '` ; [Random=' + rand + ']'); // + ' # ' + smartJ$BaseEncode.b64s_enc(rand);

}

