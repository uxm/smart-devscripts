// GO Lang
// simplejson test
// (c) 2022-2023 unix-world.org

package main

import (
	"log"

	"github.com/unix-world/smartgoext/simplejson"
)

func main() {

	json, err := simplejson.NewJson([]byte(`
{
	"test": {
		"string_array": ["asdf", "zxcv"],
		"array": [1, "2", 3],
		"arraywithsubs": [{"subkeyone": 1}],
		"aMap": { "a":2, "b":"c", "d":true, "e":null, "f":[ 0, 1, 2 ], "g":{"h":"i", "j":1234567890}, "k":1234567890, "l":0.123456789},
		"bignum": 9223372036854775807,
		"string": "simplejson",
		"bool": true
	}
}
`))

	if err != nil {
		log.Println("json format error:", err)
		return
	}

	t := json.Get("test")
	log.Println("test", t)

	s, err := t.Get("string").String()
	if err != nil {
		panic(err)
	}

	sarr, err := t.Get("string_array").StringArray()
	if err != nil {
		panic(err)
	}
	log.Println("StringArray", sarr)

	xarr, err := t.Get("array").Array()
	if err != nil {
		panic(err)
	}
	log.Println("Array", xarr)

	zarr, err := t.Get("arraywithsubs").Array()
	if err != nil {
		panic(err)
	}
	log.Println("ArrayWithSubs", zarr)

	amap, err := t.Get("aMap").Map()
	if err != nil {
		panic(err)
	}
	log.Println("aMap", amap)

	numI64, err := t.Get("bignum").Int64()
	if err != nil {
		panic(err)
	}
	log.Println("bigNum (int64)", numI64)

	numF64, err := t.Get("bignum").Float64()
	if err != nil {
		panic(err)
	}
	log.Println("bigNum (float64)", numF64)

	log.Println(s)
	//Check if a field exists
	_, ok := t.CheckGet("bignum")
	if ok {
		log.Println("exists:", json.GetPath("test", "bignum"))
	} else {
		log.Println("does not exist")
	}

}

// #END
