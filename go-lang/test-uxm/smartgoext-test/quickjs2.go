
// GoLang Sample QuickJs v2
// Execute JS Code in Golang using QuickJs VM
// (c) 2022 unix-world.org
// r.20220411.0430

package main

import (
	"fmt"
	"log"

	"runtime"
	"sort"

	"time"

	smart "github.com/unix-world/smartgo"
	assets "github.com/unix-world/smartgo/web-assets"

	"github.com/unix-world/smartgoext/quickjs"
	jsvm "github.com/unix-world/smartgoext/quickjsvm"
)


func LogToConsoleWithColors() {
	//--
	smart.ClearPrintTerminal()
	//--
//	smart.LogToStdErr("DEBUG")
	smart.LogToConsole("DEBUG", true) // colored or not
//	smart.LogToFile("DEBUG", "logs/", "json", true, true) // json | plain ; also on console ; colored or not
	//--
//	log.Println("[DEBUG] Debugging")
//	log.Println("[DATA] Data")
//	log.Println("[NOTICE] Notice")
//	log.Println("[WARNING] Warning")
//	log.Println("[ERROR] Error")
//	log.Println("[OK] OK")
//	log.Println("A log message, with no type")
	//--
} //END FUNCTION


func consoleError(ctx *quickjs.Context, this quickjs.Value, args []quickjs.Value) quickjs.Value {

	theArgs := map[string]string{}
	for kk, vv := range args {
		theArgs["arg:" + smart.ConvertIntToStr(kk)] = vv.String()
	}
	jsonArgs := smart.JsonEncode(theArgs)

	jsonStruct := smart.JsonObjDecode(jsonArgs)
//	log.Println("ERR: JS Console Error", jsonArgs, jsonStruct)
	if(jsonStruct != nil) {
		keys := make([]string, 0)
		for xx, _ := range jsonStruct {
			keys = append(keys, xx)
		}
		sort.Strings(keys)
		var txt string = ""
		for _, zz := range keys {
			txt += jsonStruct[zz].(string) + " "
		}
	//	if(txt == "This is a log message from a method inside go (this will call a go method) ") {
	//		return ctx.String("[OK] JS Console Error: " + "`" + txt + "`")
	//	} else {
	//		return ctx.String("[WARNING] JS Console Error: " + "`" + txt + "`")
	//	}
	}
//	return ctx.String("[ERROR} JS Console Error: [Empty]")
	return ctx.String(jsonArgs)
}


func consoleOk(ctx *quickjs.Context, this quickjs.Value, args []quickjs.Value) quickjs.Value {

	theArgs := map[string]string{}
	for kk, vv := range args {
		theArgs["arg:" + smart.ConvertIntToStr(kk)] = vv.String()
	}
	jsonArgs := smart.JsonEncode(theArgs)

	return ctx.String(jsonArgs)
}


func evalJsCode(loop int, js string) {

/*
	js := `
function fib(n) {
  if (n < 2) return n;
  return fib(n - 2) + fib(n - 1);
}
//console.log(fib(35));
fib(35);
`
*/

	runtime.LockOSThread()
	defer runtime.UnlockOSThread()

	jsInputData := map[string]string{"txt":smart.ConvertIntToStr(loop)+"Unicode String:		şŞţŢăĂîÎâÂșȘțȚ (05-09#"}

	jsExtendMethods := map[string]interface{}{
		"consoleError": consoleError,
		"consoleOk": consoleOk,
	}

	//--
	timerStart := time.Now()
	//--
	var timeoutJsExecSeconds uint32 = 30 // seconds
	jsEvErr, jsEvRes := jsvm.QuickJsVmRunCode(js, timeoutJsExecSeconds, 128, jsInputData, jsExtendMethods, nil)
	if(jsEvErr != "") {
		log.Println("[ERROR]", jsEvErr)
		return
	}
	//--
	durationExecJs := time.Since(timerStart)
	//--
	fmt.Println(durationExecJs, "\n", jsEvRes)
	//--

}


func myAsyncFunction(loop int, js string) <-chan int32 {
	r := make(chan int32)
	go func() {
		defer close(r)
		evalJsCode(loop, js)
		r <- int32(loop)
	}()
	return r
}


func main() {

	LogToConsoleWithColors()

	jsSettings, _ 	:= smart.SafePathFileRead("js/settings.js",  false)
	jsMain, _ 		:= smart.SafePathFileRead("js/main.js",  false)

	jsCoreUtils  := assets.ReadWebAsset("lib/js/framework/src/core_utils.js")
	jsDateUtils  := assets.ReadWebAsset("lib/js/framework/src/date_utils.js")
	jsCryptUtils := assets.ReadWebAsset("lib/js/framework/src/crypt_utils.js")
	var js string = "\n" + jsSettings + "\n\n" + jsCoreUtils + "\n\n" + jsDateUtils + "\n\n" + jsCryptUtils + "\n\n" + jsMain + "\n\n" + "main();" + "\n"
//	fmt.Println(js)

	log.Println("[NOTICE] Js Data Read DONE ...")
	time.Sleep(time.Duration(2) * time.Second)

	for i := 0; i < 10; i++ {
		r := <-myAsyncFunction(i, js)
		fmt.Println(r)
	}

}

// #end
