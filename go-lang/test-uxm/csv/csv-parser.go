
// GO Lang
// csv parser
// (c) 2023 unix-world.org

package main

import (
	"log"
	"fmt"
	"time"
	"io"
	"bufio"
	"os"
	"encoding/csv"
)

func main() {

	// Open the file
	csvfile, err := os.Open("test.csv")
	if err != nil {
		log.Fatalln("Couldn't open the csv file", err)
	}

	timerStart := time.Now()

	// Parse the file
//	r := csv.NewReader(csvfile)
	r := csv.NewReader(bufio.NewReader(csvfile))

	// Iterate through the records
	for {
		// Read each record from csv
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("Question: %s Answer %s\n", record[0], record[1])
	}

	duration := time.Since(timerStart)

	log.Println("Duration:", duration)

}


// #END
