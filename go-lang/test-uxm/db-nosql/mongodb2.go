
// GO Lang :: Sample MongoDB

package main

import (
	"context"
	"time"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

//	smart "github.com/unix-world/smartgo"
)


// bson.D is an ordered representation of a BSON document. This type should be used when the order of the elements matters, such as MongoDB command documents. If the order of the elements does not matter, an M should be used instead. A D should not be constructed with duplicate key names, as that can cause undefined server behavior.
// bson.M is an unordered representation of a BSON document. This type should be used when the order of the elements does not matter. This type is handled as a regular map[string]interface{} when encoding and decoding. Elements will be serialized in an undefined, random order. If the order of the elements matters, a D should be used instead.

const mongoConnStr string = "mongodb://localhost:27017"

var client *mongo.Client


func closeMongoDBConnection(connStr string) {

//	defer smart.PanicHandler()

	if client == nil {
		return
	}

	err := client.Disconnect(context.TODO())
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Connection to MongoDB closed", connStr) // TODO optional you can log your closed MongoDB client

}


func getMongoDBConnection(connStr string) (mCli *mongo.Client, mErr error) {

//	defer smart.PanicHandler()

	var errConn error

	if client == nil {
		ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
		client, errConn = mongo.Connect(ctx, options.Client().ApplyURI(connStr))
		if errConn != nil {
			return nil, errConn
		}
	}

	errPing := client.Ping(context.Background(), nil)
	if errPing != nil { // check the connection
		closeMongoDBConnection(connStr)
		return nil, errPing
	}

	return client, nil

}

func main() {

//	defer smart.PanicHandler()

	var mongodb *mongo.Client
	var err error

	// connect
	mongodb, err = getMongoDBConnection(mongoConnStr)
	if err != nil {
		log.Fatal(err)
	}
	// test re-connect
	mongodb, err = getMongoDBConnection(mongoConnStr)
	if err != nil {
		log.Fatal(err)
	}
	// defer close connection
	defer closeMongoDBConnection(mongoConnStr)


	collection := mongodb.Database("smart_framework").Collection("goTest")

	// FIND MANY: Several query methods return a cursor, which can be used like this:
	theQuery := bson.M{"name": bson.M{ "$in": bson.A{"pi", "qr"} } }
	ctx, _ := context.WithTimeout(context.Background(), 30*time.Second)
	cursor, err := collection.Find(ctx, theQuery)
	if err != nil {
		log.Fatal(err)
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
	//	var result bson.M
		var result bson.D
		err := cursor.Decode(&result)
		if err != nil {
			log.Fatal(err)
		}
		// do something with result....
	//	fmt.Printf("Found document (from many): %+v\n", result)
		json, jsonErr := bson.MarshalExtJSON(result, false, false)
		if(jsonErr == nil) {
			log.Println("Found document (from many) *** JSON ***:", string(json) + "\n")
		} else {
			fmt.Println("Found document (from many) *** JSON *** [ERROR]:", jsonErr, "\n")
		}
	}
	if err := cursor.Err(); err != nil {
		log.Fatal(err)
	}


	closeMongoDBConnection(mongoConnStr) // test

}

// END
