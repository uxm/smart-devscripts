module github.com/emersion/go-webdav

go 1.13

require (
	github.com/emersion/go-ical
	github.com/emersion/go-vcard
)
