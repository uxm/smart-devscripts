
// GO Lang :: SmartGo Extra :: Smart.Go.Framework
// (c) 2020-2023 unix-world.org
// r.20230517.1348 :: STABLE

// REQUIRE: go 1.17 or later
package smartgoext

//-----

const VERSION string = "v.20230517.1348"

//-----


// #END
