module github.com/unix-world/smartgoext/websocket

go 1.19

require (
	github.com/gobwas/ws v1.2.0
	github.com/mailru/easygo v0.0.0-20190618140210-3c14a0dc985f
)

require (
	github.com/gobwas/httphead v0.1.0 // indirect
	github.com/gobwas/pool v0.2.1 // indirect
	golang.org/x/sys v0.6.0 // indirect
)
