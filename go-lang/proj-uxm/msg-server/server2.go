
package main

// server2.go
// r.20221018.1858 :: STABLE

import (
	"log"
	"time"

	smart 		"github.com/unix-world/smartgo"
	wdavsrv 	"github.com/unix-world/smartgo/webdavsrv"
	msgpaksrv 	"github.com/unix-world/smartgoext/websocketmsgpak"
)


func main() {

	//--

	LogToConsoleWithColors()

	smart.HandleAbortCtrlC(msgpaksrv.WAIT_CLOSE_LIMIT_SECONDS)

	settings, iniErr = GetIniSettings(true) // server
	if((settings == nil) || (iniErr != "")) {
		FatalError("Settings Load FAILED # " + iniErr)
		return
	} //end if

	LogSetByIni(settings["log:Level"], settings["log:Format"])

	//--

	var srvHttpAddr string = settings["server:HttpAddr"]
	var srvHttpPort uint16 = uint16(smart.ParseStrAsUInt64(settings["server:HttpPort"]))
	var srvUseTLS bool = smart.ParseBoolStrAsBool(settings["server:UseTLS"])
	var srvAllowedIPsList string = settings["server:AllowedIPsList"]

	var authUsername string = settings["auth:Username"]
	var authPassword string = settings["auth:Password"]
	var sharedEncPrivKey string = settings["auth:SharedEncPrivKey"]

	var intervalMsgSeconds uint32 = uint32(smart.ParseStrAsUInt64(settings["msgpak:IntervalMsgSeconds"]))

	var wdavHttpPort uint16 = uint16(smart.ParseStrAsUInt64(settings["server:HttpWebDavPort"]))
	var wdavTimeoutSeconds uint32 = uint32(smart.ParseStrAsUInt64(settings["webdav:TimeoutSeconds"]))

	var cliTlsMode string = settings["client:TlsMode"]

	var webDavUrl string = "http"
	if(srvUseTLS) {
		webDavUrl += "s"
	} //end if
	webDavUrl += "://" + settings["server:HttpAddr"] + ":" + settings["server:HttpWebDavPort"] + "/" // the files contain the prefix: webdav/

	initServerInternalTasks(webDavUrl, intervalMsgSeconds, cliTlsMode, srvHttpAddr, srvHttpPort, authUsername, authPassword)
	initServerMsgPakClientTasks()

	if(!smart.PathExists(wdavsrv.STORAGE_DIR)) {
		smart.SafePathDirCreate(wdavsrv.STORAGE_DIR, false, false)
	} //end if

	go wdavsrv.WebdavServerRun(wdavsrv.STORAGE_DIR, srvUseTLS, msgpaksrv.CERTIFICATES_DEFAULT_PATH, srvHttpAddr, wdavHttpPort, wdavTimeoutSeconds, srvAllowedIPsList, authUsername, authPassword)
	go msgpaksrv.MsgPakServerRun("", srvUseTLS, msgpaksrv.CERTIFICATES_DEFAULT_PATH, srvHttpAddr, srvHttpPort, srvAllowedIPsList, authUsername, authPassword, sharedEncPrivKey, intervalMsgSeconds, CustomMessageHandler, srvAllowedHttpCustomCmds, cronTasks)

	for { // infinite loop
		time.Sleep(time.Duration(60) * time.Second)
		log.Println("### MessagePack Server is Running at", srvHttpAddr, "on port", srvHttpPort)
	} //end for

} //END FUNCTION

// #END
