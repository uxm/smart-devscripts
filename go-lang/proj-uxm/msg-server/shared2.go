
package main

// msghandler2.go
// r.20221018.1858 :: STABLE

import (
	"os"
	"log"
//	"time"

	smart "github.com/unix-world/smartgo"
)

const (
	theLogsDir string = "logs/"
)

func LogToConsoleWithColors() {
	//--
	if(!smart.PathExists(theLogsDir)) {
		smart.SafePathDirCreate(theLogsDir, false, false)
	} //end if
	//--
	smart.ClearPrintTerminal()
	//--
	smart.LogToConsole("DEBUG", true) // colored
//	smart.LogToConsole("NOTICE", true) // colored
	//--
//	log.Println("[DATA] Data Debugging")
//	log.Println("[DEBUG] Debugging")
//	log.Println("[NOTICE] Notice")
//	log.Println("[INFO] Info")
//	log.Println("[WARNING] Warning")
//	log.Println("[ERROR] Error")
//	log.Println("[OK] OK")
//	log.Println("A log message, with no type") // aka [INFO]
	//--
} //END FUNCTION


func LogSetByIni(logLevel string, logFormat string) {
	//--
	if(logLevel != "") {
		switch(logLevel) {
			case "DEBUG": 	fallthrough
			case "NOTICE": 	fallthrough
			case "INFO": 	fallthrough
			case "WARNING": fallthrough
			case "ERROR":
				log.Println("[INFO] Log Level changed by INI as", "[" + logLevel + "]", ";", "Format:", logFormat)
				if(logFormat == "console+file:plain") {
					smart.LogToFile(logLevel, theLogsDir, "plain", true, true)
				} else if(logFormat == "file:plain") {
					smart.LogToFile(logLevel, theLogsDir, "plain", false, false)
				} else if(logFormat == "console+file:json") {
					smart.LogToFile(logLevel, theLogsDir, "json", true, true)
				} else if(logFormat == "file:json") {
					smart.LogToFile(logLevel, theLogsDir, "json", false, false)
				} else { // console
					smart.LogToConsole(logLevel, true)
				} //end if else
				break;
			default:
				log.Println("[INFO] Log Level and Format not changed by INI")
		} //end switch
	} //end if
	//--
} //END FUNCTION


func FatalError(logMessages ...interface{}) {
	//--
	log.Fatal("[ERROR] ", logMessages) // standard logger
	os.Exit(1)
	//--
} //END FUNCTION


var settings map[string]string = nil
var iniErr string = ""

func GetIniSettings(isServer bool) (theData map[string]string, parseErr string) {
	//--
	var iniFilePath string = smart.PathGetCurrentExecutableDir()
	if(smart.StrStartsWith(iniFilePath, "/tmp/")) {
		iniFilePath = smart.PathGetAbsoluteFromRelative("./")
	} //end if
	iniFilePath = smart.PathAddDirLastSlash(iniFilePath) + "settings.ini"
	log.Println("[NOTICE] Load Settings: `" + iniFilePath + "`")
	//--
	var iniDataReq []string
	iniDataReq = append(iniDataReq, "log:Level")
	iniDataReq = append(iniDataReq, "log:Format")
	iniDataReq = append(iniDataReq, "auth:Username")
	iniDataReq = append(iniDataReq, "auth:Password")
	iniDataReq = append(iniDataReq, "auth:SharedEncPrivKey")
	iniDataReq = append(iniDataReq, "msgpak:IntervalMsgSeconds")
	iniDataReq = append(iniDataReq, "server:HttpAddr")
	iniDataReq = append(iniDataReq, "server:HttpPort")
	iniDataReq = append(iniDataReq, "server:UseTLS")
	iniDataReq = append(iniDataReq, "server:HttpWebDavPort")
	iniDataReq = append(iniDataReq, "webdav:TimeoutSeconds")
	if(isServer == true) {
		iniDataReq = append(iniDataReq, "server:AllowedIPsList")
		iniDataReq = append(iniDataReq, "server-tasks:TaskStatsTiming")
		iniDataReq = append(iniDataReq, "server-tasks:TaskStatsSaveUrlUri")
		iniDataReq = append(iniDataReq, "server-tasks:TaskStatsSaveUrlUser")
		iniDataReq = append(iniDataReq, "server-tasks:TaskStatsSaveUrlPass")
		iniDataReq = append(iniDataReq, "server-tasks:TaskPgBackupTiming")
	} //end if else
	iniDataReq = append(iniDataReq, "client:TlsMode")
	//iniDataReq = nil
	//--
	setts, err := smart.SafePathIniFileReadAndParse(iniFilePath, true, iniDataReq)
	if(err != "") {
		return nil, "INI Settings # `" + iniFilePath + "`: " + err
	} //end if
	//--
	if(setts == nil) {
		return nil, "INI Settings # `" + iniFilePath + "`: " + "No Settings Found"
	} //end if
	//--
	for k, v := range setts {
		if(smart.StrTrimWhitespaces(v) == "") {
			return nil, "INI Settings # `" + iniFilePath + "`: " + "A Required Setting `" + k + "` is Empty !"
		} //end if
	} //end for
	//--
	return setts, ""
	//--
} //END FUNCTION


// #END
