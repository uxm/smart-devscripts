#!/bin/sh

# r.20221018.1858 :: STABLE

if [ -f server ]; then
    rm server
fi

if [ -f client ]; then
    rm client
fi

go build -o server server2.go shared2.go msghandler2-server.go srv-task-pg-backup.go
go build -o client client2.go shared2.go msghandler2-client.go cli-task-pg-restore.go

echo "======= Build DONE: Server and Client [PROD]"
sleep 2
