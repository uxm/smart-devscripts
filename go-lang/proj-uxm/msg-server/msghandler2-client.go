
package main

// msghandler2.go
// r.20221020.1418 :: STABLE

import (
	"log"
	"time"
	"context"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	smart "github.com/unix-world/smartgo"
	smarthttputils "github.com/unix-world/smartgo/web-httputils"
)


//=======


const (
	mongoConnStr string 		= "mongodb://127.0.0.1:27017"
	mongoDbName  string 		= "s112_data"
	mongoColName string 		= "DataCollect"
	mongoCtxConnTimeout uint16 	= 20
	mongoCtxQryTimeout uint16 	= 60
	mongoDocsLimit int64		= 5000
)

func closeMongoDBConnection(connStr string, client *mongo.Client) string {
	if client == nil {
		return "[NOTICE] Connection to MongoDB is already closed: " + connStr
	}
	err := client.Disconnect(context.TODO())
	if err != nil {
		return "[ERROR] Failed to close Connection to MongoDB: " + connStr + " # " + err.Error()
	}
	return "[INFO] Connection to MongoDB closed: " + connStr
} //END FUNCTION


func getMongoDBConnection(connStr string) (mCli *mongo.Client, mErr error) {
	var client *mongo.Client
	var errConn error
	ctx, _ := context.WithTimeout(context.Background(), time.Duration(mongoCtxConnTimeout) * time.Second)
	client, errConn = mongo.Connect(ctx, options.Client().ApplyURI(connStr))
	if(errConn != nil) {
		return nil, errConn
	} //end if
	errPing := client.Ping(context.Background(), nil)
	if(errPing != nil) { // check the connection
		closeMongoDBConnection(connStr, client)
		return nil, errPing
	} //end if
	return client, nil
} //END FUNCTION


func mongoDbSetStats(mConnStr string, mDbName string, mColName string, arrIDs []string) (qryErr string) {
	//--
	defer smart.PanicHandler()
	//--
	if((arrIDs == nil) || (len(arrIDs) <= 0)) {
		return "[WARNING] No records Provided"
	} //end if
	//--
	mongodb, conErr := getMongoDBConnection(mConnStr)
	if(conErr != nil) {
		return "[ERROR] Failed to Connect to MongoDB: " + mConnStr + " # " + conErr.Error()
	} //end if
	if(mongodb == nil) {
		return "[ERROR] Failed to Connect to MongoDB: " + mConnStr + " # Connector is NULL"
	} //end if
	defer closeMongoDBConnection(mConnStr, mongodb)
	//--
	collection := mongodb.Database(mDbName).Collection(mColName)
	ctx, _ := context.WithTimeout(context.Background(), time.Duration(mongoCtxQryTimeout) * time.Second)
	//--
	var success uint64 = 0
	for i := 0; i < len(arrIDs); i++ {
		delQuery := bson.D{{"_id", string(arrIDs[i])}}
		deleteResult, err := collection.DeleteOne(ctx, delQuery)
		if(err != nil) {
			log.Println("[ERROR] FAILED to DELETE a STATS record with UID:", string(arrIDs[i]), "# Err-Message:", err)
		} else {
			success++
			log.Println("[DEBUG] SUCCESSFULLY DELETED STATS record with UID:", string(arrIDs[i]), "Result:", deleteResult)
		} //end if else
	} //end for
	log.Println("[INFO] DELETED", success, "STATS records")
	return ""
	//--
} //END FUNCTION


func mongoDbGetStats(mConnStr string, mDbName string, mColName string) (jsonStr string, qryErr string) {
	//--
	defer smart.PanicHandler()
	//--
	mongodb, conErr := getMongoDBConnection(mConnStr)
	if(conErr != nil) {
		return "", "[ERROR] Failed to Connect to MongoDB: " + mConnStr + " # " + conErr.Error()
	} //end if
	if(mongodb == nil) {
		return "", "[ERROR] Failed to Connect to MongoDB: " + mConnStr + " # Connector is NULL"
	} //end if
	defer closeMongoDBConnection(mConnStr, mongodb)
	//--
	collection := mongodb.Database(mDbName).Collection(mColName)
	ctx, _ := context.WithTimeout(context.Background(), time.Duration(mongoCtxQryTimeout) * time.Second)
	//--
	options := options.Find()
	options.SetSort(bson.D{{"dt", 1}, {"_id", 1}}) // sort on `dt` ASC, `_id` ASC
	options.SetLimit(mongoDocsLimit) // set read limit
	//--
	theQuery := bson.D{}
	cursor, cursorErr := collection.Find(ctx, theQuery, options)
	if(cursorErr != nil) {
		return "", "[ERROR] Failed to Select the Collection `" + mColName + "` at MongoDB: " + mConnStr + " # " + cursorErr.Error()
	} //end if
	defer cursor.Close(ctx)
	if(cursor == nil) {
		return "", "[ERROR] to Get the Cursor for Collection `" + mColName + "` at MongoDB: " + mConnStr + " # Cursor is NULL"
	} //end if
	var jsonExpStr string = ""
	var jsonArr []string = nil
	var recNum int64 = 0;
	for cursor.Next(ctx) {
	//	var result bson.M // unordered
		var result bson.D // ordered
		decodeErr := cursor.Decode(&result)
		if(decodeErr != nil) {
			return "", "[ERROR] Failed to Decode a Record JSON from Collection `" + mColName + "` at MongoDB: " + mConnStr + " # " + decodeErr.Error()
		} //end if
		// do something with result....
		json, jsonErr := bson.MarshalExtJSON(result, false, false)
		if(jsonErr != nil) {
			return "", "[ERROR] Failed to Marshal a Record JSON from Collection `" + mColName + "` at MongoDB: " + mConnStr + " # " + jsonErr.Error()
		} //end if
		jsonArr = append(jsonArr, string(json))
		recNum++
		if(recNum >= mongoDocsLimit) {
			break
		} //end if
	} //end for
	if CursorCheckErr := cursor.Err(); CursorCheckErr != nil {
		return "", "[ERROR] The Cursor for Collection `" + mColName + "` at MongoDB: " + mConnStr + " # Fatal Check Error: " + " # " + CursorCheckErr.Error()
	} //end if
	//--
	jsonExpStr = "[" + "\n" + smart.Implode(",\n", jsonArr) + "\n" + "]"
	jsonArr = nil
	//--
	return jsonExpStr, ""
	//--
} //END FUNCTION


func cliHandleCmdGetStats() (jsonStr string, qryErr string) {
	//--
	theJsonStr, theQryErr := mongoDbGetStats(mongoConnStr, mongoDbName, mongoColName)
	//--
	return theJsonStr, theQryErr
	//--
} //END FUNCTION


//=======


func CustomMessageHandler(isServer bool, id string, remoteId string, cmd string, data string, authUsername string, authPassword string) (string, string) {
	//--
	defer smart.PanicHandler()
	//--
	var answerMsg string = ""
	var answerData string = ""
	//--
	if(isServer != true) { // client
		//--
		switch(cmd) {
			case "<WEBDAV.GET.DB>":
				const dwnPath string = "downloads/"
				if(uxmIsPgRestoreTaskRunning != false) {
					answerMsg = "<ERR:WEBDAV.GET.DB>"
					answerData = "Restoring Process is currently Running ..."
				} else {
					if(smart.PathExists(dwnPath)) { // pre-cleanup
						if(smart.PathIsDir(dwnPath)) {
							smart.SafePathDirDelete(dwnPath, false)
						} else if(smart.PathIsFile(dwnPath)) {
							smart.SafePathFileDelete(dwnPath, false)
						} //end if
					} //end if
					if(!smart.PathExists(dwnPath)) {
						smart.SafePathDirCreate(dwnPath, false, false)
					} //end if
					if(!smart.PathIsDir(dwnPath)) {
						answerMsg = "<ERR:WEBDAV.GET.DB>"
						answerData = "Download Folder Not Exists: " + dwnPath
					} else {
						tmpIniSett, tmpIniErr := smart.IniContentParse(data, nil)
						if(tmpIniErr != "") {
							answerMsg = "<ERR:WEBDAV.GET.DB>"
							answerData = "WebDAV INI Parse ERR: " + tmpIniErr
						} else {
							if(smart.PathIsFile(dwnPath + tmpIniSett["webdav:file"])) {
								smart.SafePathFileDelete(dwnPath + tmpIniSett["webdav:file"], false)
							} //end if
							if(smart.PathIsFile(dwnPath + tmpIniSett["webdav:cfile"])) {
								smart.SafePathFileDelete(dwnPath + tmpIniSett["webdav:cfile"], false)
							} //end if
							httpCResult := smarthttputils.HttpClientDoRequestGET(tmpIniSett["webdav:uri"] + tmpIniSett["webdav:cfile"], "", true, nil, 0, 255, 0, authUsername, authPassword)
							var tmpSizeArchStr string = smart.StrTrimWhitespaces(httpCResult.BodyData)
							var tmpSizeArchInt64 int64 = smart.ParseStrAsInt64(tmpSizeArchStr)
							if(httpCResult.Errors != "") {
								answerMsg = "<ERR:WEBDAV.GET.DB>"
								answerData = "WebDAV GET SIZE ERR: " + httpCResult.Errors
							} else if(httpCResult.HttpStatus != 200) {
								answerMsg = "<ERR:WEBDAV.GET.DB>"
								answerData = "WebDAV GET SIZE ERR: [Status=" + smart.ConvertIntToStr(httpCResult.HttpStatus) + "]"
							} else if((tmpSizeArchInt64 <= 0) || (tmpSizeArchInt64 > 77888777888)) { // max 78GB
								answerMsg = "<ERR:WEBDAV.GET.DB>"
								answerData = "WebDAV GET SIZE ERR: [Size=" + tmpSizeArchStr + "]"
							} else {
								httpResult := smarthttputils.HttpClientDoRequestDownloadFile(dwnPath, "GET", tmpIniSett["webdav:uri"] + tmpIniSett["webdav:file"], "", true, nil, nil, uint32(smart.ParseStrAsUInt64(settings["webdav:TimeoutSeconds"])), 0, authUsername, authPassword)
								if(httpResult.Errors != "") {
									answerMsg = "<ERR:WEBDAV.GET.DB>"
									answerData = "WebDAV GET ERR: " + httpResult.Errors
								} else if(httpResult.HttpStatus != 200) {
									answerMsg = "<ERR:WEBDAV.GET.DB>"
									answerData = "WebDAV GET ERR: [" + smart.ConvertIntToStr(httpResult.HttpStatus) + "]"
								} else {
									var dwnFile string = dwnPath + smart.PathBaseName(tmpIniSett["webdav:file"])
									fileSize, errFSize := smart.SafePathFileGetSize(dwnFile, false)
									if(errFSize != "") {
										answerMsg = "<ERR:WEBDAV.GET.DB>"
										answerData = "WebDAV GET SIZE ERR: # " + errFSize + " # " + dwnFile
									} else if(fileSize != tmpSizeArchInt64) {
										answerMsg = "<ERR:WEBDAV.GET.DB>"
										answerData = "WebDAV GET SIZE Compare ERR: # " + smart.ConvertInt64ToStr(fileSize) + "/" + smart.ConvertInt64ToStr(tmpSizeArchInt64) + "@" + tmpSizeArchStr + " # " + dwnFile
									} else {
										smart.SafePathFileWrite(dwnFile + ".size", "w", false, tmpSizeArchStr)
										log.Println("[OK] %%%%%%% %%%%%%% %%%%%%% Download DB Archive: COMPLETED ... %%%%%%%")
										answerMsg = "<OK:WEBDAV.GET.DB>"
										answerData = dwnFile
									} //end if else
								} //end if else
							} //end if else
						} //end if else
					} //end if else
				} //end if else
				break
			case "<STATS>": // return STATS to server as: OK:STATS
				jsonStr, errStr := cliHandleCmdGetStats()
				if(errStr != "") {
					log.Println("[ERROR] Execute Command Failed: " + cmd + " # " + errStr)
					answerMsg = "<ERR:STATS>"
					answerData = errStr
				} else {
					answerMsg = "<OK:STATS>"
					answerData = jsonStr
				} //end if
				break
			case "<OK:STATS>": // final reply from server on STATS
				log.Println("[OK] Import Stats ... DONE")
				data = smart.StrTrimWhitespaces(data)
				var reply string  = ""
				if(data == "") {
					reply = "####### EMPTY: NO DATA #######"
				} else {
					reply = data + "\n\n\n"
					var arrIDs []string = nil
					var theRecId string = ""
					var theLine string = ""
					const okLineStart string = "[OK]" + "\t" + "-1" + "\t"
					arrReply := smart.Explode("\n", data)
					for i := 0; i < len(arrReply); i++ {
						theLine = smart.StrTrimWhitespaces(arrReply[i])
						if(theLine != "") {
							if(smart.StrIStartsWith(theLine, okLineStart)) {
								theRecId = smart.StrTrimWhitespaces(smart.StrSubstr(theLine, len(okLineStart), -1))
								arrIDs = append(arrIDs, theRecId)
							} //end if
						} //end if
					} //end for
					arrReply = nil
					theLine = ""
					theRecId = ""
					log.Println("[DATA] Stats Reply OK ID List:", arrIDs)
					log.Println("[DEBUG] Stats Reply OK ID List Length:", len(arrIDs))
					errDel := mongoDbSetStats(mongoConnStr, mongoDbName, mongoColName, arrIDs)
					if(errDel != "") {
						log.Println(errDel, "-", "Delete STATS Not Completed ...")
					} //end if
				} //end if
				smart.SafePathFileWrite(theLogsDir + "stats-client.log", "w", false, reply) // (isSuccess bool, errMsg string)
				break
			case "<ERR:STATS>": // final reply from server on STATS
				log.Println("[WARNING] Import Stats ... FAILED")
				var reply string = data + "\n\n\n"
				smart.SafePathFileWrite(theLogsDir + "stats-client-err.log", "w", false, reply) // (isSuccess bool, errMsg string)
				break
			default: // unhandled
				answerMsg = "<ERR:UNHANDLED> No Client Handler for command: `" + cmd + "` ..." // return an error answer
		} //end switch
		//--
	} //end if
	//--
	return answerMsg, answerData
	//--
} //END FUNCTION


//=======


// #END
