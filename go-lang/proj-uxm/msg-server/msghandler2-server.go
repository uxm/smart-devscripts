
package main

// msghandler2.go
// r.20221018.1858 :: STABLE

import (
	"log"

	smart "github.com/unix-world/smartgo"
	smarthttputils "github.com/unix-world/smartgo/web-httputils"

	msgpaksrv "github.com/unix-world/smartgoext/websocketmsgpak"
)

//=======

var srvAllowedHttpCustomCmds map[string]bool = map[string]bool{
	"STATS": true, // if true can be schedduled also via HTTP(S) tasks manager, else only by cron tasks manager
	"WEBDAV.GET.DB": true,
}

var cronTasks []msgpaksrv.CronMsgTask
func initServerMsgPakClientTasks() {
	log.Println("[DEBUG]", "initServerMsgPakClientTasks # <STATS> Task Timing: `" + settings["server-tasks:TaskStatsTiming"] + "`")
	cronTasks = []msgpaksrv.CronMsgTask{ // list of schedduled commands to send to clients
		msgpaksrv.CronMsgTask{ settings["server-tasks:TaskStatsTiming"],   "STATS", "", }, // run scheduled
	//	msgpaksrv.CronMsgTask{ "*/2 * * * *", "PADURLS", "XYZ", }, 	// run scheduled (every 2 minutes)
	}
}

func CustomMessageHandler(isServer bool, id string, remoteId string, cmd string, data string, authUsername string, authPassword string) (string, string) {
	//--
	defer smart.PanicHandler()
	//--
	var answerMsg string = ""
	var answerData string = ""
	//--
	if(isServer == true) { // server
		//--
		switch(cmd) {
			case "<OK:DB.RESTORE>":
				log.Println("[OK] DB Restore")
				log.Println("[DATA] DB Restore Details:", data)
				break
			case "<ERR:WEBDAV.GET.DB>":
				log.Println("[WARNING] WebDAV Get DB Backup Failed:", data)
				break
			case "<OK:WEBDAV.GET.DB>":
				log.Println("[OK] WebDAV Get DB Backup")
				log.Println("[DATA] WebDAV Get DB Backup Details:", data)
				break
			case "<STATS>": // ask STATS from Client
				answerMsg = "<STATS>"
				break
			case "<ERR:STATS>":
				log.Println("[WARNING] Get Stats Failed:", data)
				break
			case "<OK:STATS>": // send stats package to PHP and return back to client: OK:STATS / ERR:STATS
				var reqArr map[string][]string = map[string][]string{
					"pak": { data },
				}
				log.Println("[DEBUG]", "CustomMessageHandler @ <OK:STATS> # URL Info: `" + settings["server-tasks:TaskStatsSaveUrlUri"] + "` # `" + settings["server-tasks:TaskStatsSaveUrlUser"] + ":" + settings["server-tasks:TaskStatsSaveUrlPass"] + "`")
				httpResult := smarthttputils.HttpClientDoRequestPOST(settings["server-tasks:TaskStatsSaveUrlUri"], "", true, nil, reqArr, 0, smarthttputils.HTTP_CLI_MAX_BODY_READ_SIZE, smarthttputils.HTTP_CLI_MAX_REDIRECTS, settings["server-tasks:TaskStatsSaveUrlUser"], settings["server-tasks:TaskStatsSaveUrlPass"])
				if(httpResult.Errors != "") {
					log.Println("[ERROR] Import Stats Failed:", httpResult.Errors)
					answerMsg = "<ERR:STATS>"
					answerData = httpResult.Errors
				} else if(httpResult.HttpStatus != 202) {
					if(httpResult.HttpStatus == 203) { // empty content/data
						answerMsg = "<OK:STATS>"
					} else {
						answerMsg = "<ERR:STATS>"
					} //end if else
					if(httpResult.MimeType == "text/plain") {
						log.Println("[WARNING] Import Stats Failed:", httpResult.HttpStatus, httpResult.BodyData)
					} else {
						log.Println("[ERROR] Import Stats Failed:", httpResult.HttpStatus)
					} //end if else
					if((httpResult.HttpStatus == 203) || (httpResult.HttpStatus == 208)) {
						answerData = smart.ConvertIntToStr(httpResult.HttpStatus) + "\n" + httpResult.BodyData
					} //end if
				} else {
					answerMsg = "<OK:STATS>"
					answerData = httpResult.BodyData
				} //end if else
				var reply string = "[HttpStatus=" + smart.ConvertIntToStr(httpResult.HttpStatus) + "]" + "\n"
				smart.SafePathFileWrite(theLogsDir + "stats-server.log", "w", false, reply) // (isSuccess bool, errMsg string)
				break
			default: // unhandled
				answerMsg = "<ERR:UNHANDLED> No Server Handler for command: `" + cmd + "` ..." // return an error answer
		} //end switch
		//--
	} //end if
	//--
	return answerMsg, answerData
	//--
} //END FUNCTION


//=======


// #END
