
// GO Lang :: M-DataCenter :: Pg-Backup
// (c) 2020-2022 unix-world.org
// r.20221018.1858 :: STABLE

package main

// DEPENDS executables: 7za, psql

import (
	"log"
	"time"

	_ "github.com/lib/pq"
	"github.com/jmoiron/sqlx"

	smart "github.com/unix-world/smartgo"
)


const (
	CMD_TIMEOUT uint = 3600 * 4 // max 4h per cmd (may need to change this)

	PG_HOST string = "127.0.0.1"
	PG_PORT string = "5432"
	PG_USER string = "pgsql"
	PG_PASS string = "pgsql"
	PG_DB   string = "s112_r"

	PG_TEMPLATE_DB string = "template0"
	PG_MASTER_DB   string = "postgres"

	PG_PSQL_EXECUTABLE string 	= "/opt/msg-server/bin/psql"
	SEVEN_ZIP_EXECUTABLE string = "/opt/msg-server/bin/7za"

	BKP_RESTORE_FOLDER string 	= "downloads/"  // this folder is where the 7zip archive must be found
	BKP_UNARCHIVE_FOLDER string = "db-restore/" // this is the folder where will unarchive
	BKP_ARCHIVE_FOLDER string 	= "db-backup/"  // this folder must exists inside of the 7zip archive and here will be decompressed the archive
)


//--

type DbExists struct {
	Exists string `db:"the_db_exists"`
}

//--

var uxmIsPgRestoreTaskRunning bool = false

//--

func RunRestoreTask() (restoreErr string, archFileName string, databaseName string) {

	//--
	defer smart.PanicHandler()
	//--

	//--
	uxmIsPgRestoreTaskRunning = true
	//--
	defer func() {
		uxmIsPgRestoreTaskRunning = false
	}()
	//--

	//--
	var DateTimeStartUtc string = smart.DateNowUtc()
	//--
	log.Println("[INFO] ##### M-DataCenter :: PostgreSQL Restore Task :: " + DateTimeStartUtc + " #####")
	//--

	//--
	getSzFileName := func(bkpFile string) string {
		//--
		var theSzFile string = bkpFile + ".size"
		//--
		return theSzFile
		//--
	} //end function
	//--
	checkFileSize := func(bkpFile string) (isOk bool, errMsg string) {
		//--
		var theSzFile string = getSzFileName(bkpFile)
		//--
		if(!smart.PathExists(theSzFile)) {
			return false, "The Data Backup Size File: `" + bkpFile + "` # Does NOT Exist"
		} //end if
		//--
		fSize, errMsg := smart.SafePathFileGetSize(bkpFile, false)
		if((errMsg != "") || (fSize <= 0)) {
			return false, "Could Not Get the Size of the Data Backup Size File Content: `" + bkpFile + "` # " + errMsg
		} //end if
		//--
		fRealSize, errRealMsg := smart.SafePathFileRead(theSzFile, false)
		fRealSize = smart.StrTrimWhitespaces(fRealSize)
		if((errRealMsg != "") || (fRealSize == "")) {
			return false, "Could Not Read the Data Backup Size File Content: `" + theSzFile + "` # " + errRealMsg
		} //end if
		//--
		var fStrSize string = smart.ConvertInt64ToStr(fSize)
		if(fStrSize != fRealSize) {
			return false, "Invalid Data Backup Size File Content: " + fRealSize + " != " + fStrSize
		} //end if
		//--
		return true, ""
		//--
	} //end function
	//--
	getMd5FileName := func(bkpFile string) string {
		//--
		var theMd5ChskumFile string = bkpFile + ".md5"
		//--
		return theMd5ChskumFile
		//--
	} //end function
	//--
	checkFileMd5 := func(bkpFile string) (isOk bool, errMsg string) {
		//--
		var theMd5File string = getMd5FileName(bkpFile)
		//--
		md5Sum, errMsg := smart.SafePathFileMd5(bkpFile, false)
		if((errMsg != "") || ((md5Sum == "") || (len(md5Sum) != 32))) {
			return false, "Failed to get the MD5 sum of File: `" + bkpFile + "` # " + errMsg
		} //end if
		//--
		fMd5ContentsRead, errMd5FileRead := smart.SafePathFileRead(theMd5File, false)
		fMd5ContentsRead = smart.StrTrimWhitespaces(fMd5ContentsRead)
		if((errMd5FileRead != "") || (fMd5ContentsRead == "")) {
			return false, "Could Not Read The Data Backup FileMd5: `" + theMd5File + "` # " + errMd5FileRead
		} //end if
		//--
		var theChecksumFileBaseName string = bkpFile
		if(smart.StrContains(theChecksumFileBaseName, "/")) {
			theChecksumFileBaseName = smart.PathBaseName(theChecksumFileBaseName)
		} //end if
		var expectedContent string = "MD5 (" + theChecksumFileBaseName + ") = " + md5Sum
		if(expectedContent != fMd5ContentsRead) {
			return false, "Invalid Data Backup FileMd5: `" + expectedContent + "` != `" + fMd5ContentsRead + "`"
		} //end if
		//--
		return true, ""
		//--
	} //end function
	//--

	//--
	if(
		(smart.PathIsEmptyOrRoot(BKP_RESTORE_FOLDER)) ||
		(smart.PathIsAbsolute(BKP_RESTORE_FOLDER)) ||
		(smart.PathIsBackwardUnsafe(BKP_RESTORE_FOLDER)) ||
		(!smart.StrEndsWith(BKP_RESTORE_FOLDER, "/"))) {
			return "Invalid Archive Dir: `" + BKP_RESTORE_FOLDER + "`", "", ""
	} //end if
	//--
	if((!smart.PathExists(BKP_RESTORE_FOLDER)) || (!smart.PathIsDir(BKP_RESTORE_FOLDER))){
		// NOTICE, NOT ERROR !
		return "Skip Task: The Archive Dir does NOT Exists", "", ""
	} //end if
	//--
	if(
		(smart.PathIsEmptyOrRoot(BKP_UNARCHIVE_FOLDER)) ||
		(smart.PathIsAbsolute(BKP_UNARCHIVE_FOLDER)) ||
		(smart.PathIsBackwardUnsafe(BKP_UNARCHIVE_FOLDER)) ||
		(!smart.StrEndsWith(BKP_UNARCHIVE_FOLDER, "/"))) {
			return "Invalid Backup Dir in Archive: `" + BKP_UNARCHIVE_FOLDER + "`", "", ""
	} //end if
	//--

	//--
	isReClrDirBkOk, errReClrDirBk := smart.SafePathDirDelete(BKP_UNARCHIVE_FOLDER, false)
	if((isReClrDirBkOk != true) || (errReClrDirBk != "") || (smart.PathExists(BKP_UNARCHIVE_FOLDER))) {
		return "Restore Folder Cleanup (before unarchiving) FAILED: # " + errReClrDirBk, "", ""
	} //end if
	//--

	//--
	scanOk, errScanMsg, _, arrFiles := smart.SafePathDirScan(BKP_RESTORE_FOLDER, false, false)
	if((scanOk != true) || (errScanMsg != "")) {
		return "Failed to Scan the Archive Dir: `" + BKP_RESTORE_FOLDER + "` :: " + errScanMsg, "", ""
	} //end if
	//--
	var archTestFile string = ""
	for _, file := range arrFiles {
		if(smart.StrEndsWith(file, ".7z")) {
			if(file > archTestFile) { // get the latest one
				archTestFile = file
			} //end if
		} //end if
	} //end for
	//--
	if(
		(archTestFile == "") ||
		(!smart.PathExists(archTestFile)) ||
		(!smart.PathIsFile(archTestFile)) ||
		(!smart.StrEndsWith(archTestFile, ".7z"))) {
			return "Failed to Find the 7Zip Archive File into: `" + BKP_RESTORE_FOLDER + "`", archTestFile, ""
	} //end if
	archDoneFile := archTestFile + ".done-restore"
	if((smart.PathExists(archDoneFile)) || (smart.PathIsFile(archDoneFile))) {
		return "Skip Task: The Archive Was already processed: `" + archDoneFile + "`", archTestFile, ""
	} //end if
	//--
	archSizeTestFile := archTestFile + ".size"
	//--
	if(
		(archSizeTestFile == "") ||
		(!smart.PathExists(archSizeTestFile)) ||
		(!smart.PathIsFile(archSizeTestFile)) ||
		(!smart.StrEndsWith(archSizeTestFile, ".7z.size"))) {
			bkpMTime, errMTime := smart.SafePathGetMTime(archTestFile, false)
			if(errMTime != "") {
				return "Failed to Get the 7Zip Archive File MTime: `" + BKP_RESTORE_FOLDER + "` # " + errMTime, archTestFile, ""
			} //end if
			var timeUnixNow int64 = int64(time.Now().Unix())
			if(int64(bkpMTime + int64(CMD_TIMEOUT)) < timeUnixNow) {
				okDelete, errDelete := smart.SafePathFileDelete(archTestFile, false)
				if((!okDelete) || (errDelete != "")) {
					return "Failed to Delete the --STALLED-- 7Zip Archive File: `" + BKP_RESTORE_FOLDER + "` # " + errDelete, archTestFile, ""
				} //end if
			} //end if

			return "Failed to Find the 7Zip Archive Size File into: `" + BKP_RESTORE_FOLDER + "`", archTestFile, ""
	} //end if
	//--

	//--
	log.Println("[DEBUG] ## Backup Archive Dir was Found at: `" + BKP_RESTORE_FOLDER + "` ## :: " + archTestFile)
	//--
	theFSize, errFSizeMsg := smart.SafePathFileGetSize(archTestFile, false)
	if((theFSize <= 0) || (errFSizeMsg != "")) {
		return "Failed to Get The FileSize of the 7Zip Archive File: `" + archTestFile + "` :: " + errFSizeMsg, archTestFile, ""
	} //end if
	//--
	theChkSize, errRdSizeFile := smart.SafePathFileRead(archSizeTestFile, false)
	theChkSize = smart.StrTrimWhitespaces(theChkSize)
	if(
		(theChkSize == "") ||
		(!smart.StrRegexMatchString(`^[0-9]+$`, theChkSize)) ||
		(errRdSizeFile != "")) {
			return "Invalid 7Zip Archive Size File Content: `" + archSizeTestFile + "` :: " + errRdSizeFile, archTestFile, ""
	} //end if
	//--
	var theIChkSize int64 = smart.ParseStrAsInt64(theChkSize)
	if(theIChkSize <= 0) {
		return "Invalid 7Zip Archive Size File Int64 Content: `" + archSizeTestFile + "` :: " + smart.ConvertInt64ToStr(theIChkSize), archTestFile, ""
	} //end if
	//--
	if(theIChkSize != theFSize) {
		return "Invalid 7Zip Archive File Size Check # " + smart.ConvertInt64ToStr(theFSize) + " :: " + smart.ConvertInt64ToStr(theIChkSize), archTestFile, ""
	} //end if
	//--

	//--
	log.Println("[DEBUG] ### Archive Size Check :: OK: " + theChkSize + " Bytes ###")
	//--

	//--
	log.Println("[NOTICE] ========== Un-Archiving (7-Zip): START ==========")
	log.Println("[DEBUG] 7za e: `" + archTestFile + "` > `" + BKP_RESTORE_FOLDER + "`")
	//--
	isSuccess, outStd, errStd := smart.ExecTimedCmd(CMD_TIMEOUT, "capture", "capture", "", "", SEVEN_ZIP_EXECUTABLE, "e", "-y", archTestFile, "-o" + BKP_UNARCHIVE_FOLDER, BKP_ARCHIVE_FOLDER + "*", "-r", "-bb1")
	if(outStd != "") {
		log.Println("[DATA] 7za Output:", smart.StrReplaceAll(smart.StrReplaceAll(smart.StrTrimWhitespaces(outStd), "\n\n\n", "\n"), "\n\n", "\n"))
	} //end if
	if((isSuccess != true) || (errStd != "")) {
		return "7-Zip Archiver encountered Errors / StdErr:\n`" + errStd + "`\n", archTestFile, ""
	} //end if
	//--
	log.Println("[NOTICE] ========== Un-Archiving (7Zip): END ==========")
	//--
	if((!smart.PathExists(BKP_UNARCHIVE_FOLDER)) || (!smart.PathIsDir(BKP_RESTORE_FOLDER))) {
		return "Unarchive FAILED: The Data Backup Folder cannot be found after unarchiving: `" + BKP_UNARCHIVE_FOLDER + "`", archTestFile, ""
	} //end if
	//--

	//--
	log.Println("[NOTICE] ========== Checking Files Integrity: START ==========")
	//--
	if(
		(!smart.PathIsFile(BKP_UNARCHIVE_FOLDER + "db-schema.sql")) ||
		(!smart.PathIsFile(BKP_UNARCHIVE_FOLDER + "db-schema.sql.md5")) ||
		(!smart.PathIsFile(BKP_UNARCHIVE_FOLDER + "db-schema.sql.size")) ||
		(!smart.PathIsFile(BKP_UNARCHIVE_FOLDER + "db-data.sql")) ||
		(!smart.PathIsFile(BKP_UNARCHIVE_FOLDER + "db-data.sql.md5")) ||
		(!smart.PathIsFile(BKP_UNARCHIVE_FOLDER + "db-data.sql.size")) ||
		(!smart.PathIsFile(BKP_UNARCHIVE_FOLDER + "db-pgdump.ok"))) {
			return "Unarchive FAILED: The Data Backup Folder is missing some required files after unarchiving: `" + BKP_UNARCHIVE_FOLDER + "`", archTestFile, ""
	} //end if
	//--
	chkSchemaFSize, errSchemaFSize := checkFileSize(BKP_UNARCHIVE_FOLDER + "db-schema.sql")
	if(chkSchemaFSize != true) {
		return "Unarchive FAILED (Schema): `" + errSchemaFSize + "`", archTestFile, ""
	} //end if
	//--
	chkDataFSize, errDataFSize := checkFileSize(BKP_UNARCHIVE_FOLDER + "db-data.sql")
	if(chkDataFSize != true) {
		return "Unarchive FAILED (Data): `" + errDataFSize + "`", archTestFile, ""
	} //end if
	//--
	chkSchemaFMd5, errSchemaFMd5 := checkFileMd5(BKP_UNARCHIVE_FOLDER + "db-schema.sql")
	if(chkSchemaFMd5 != true) {
		return "Unarchive Checksum FAILED (Schema): `" + errSchemaFMd5 + "`", archTestFile, ""
	} //end if
	//--
	chkDataFMd5, errDataFMd5 := checkFileMd5(BKP_UNARCHIVE_FOLDER + "db-data.sql")
	if(chkDataFMd5 != true) {
		return "Unarchive Checksum FAILED (Data): `" + errDataFMd5 + "`", archTestFile, ""
	} //end if
	//--
	log.Println("[DEBUG] ### Control File, Sizes and MD5 Checksums: OK")
	//--
	log.Println("[NOTICE] ========== Checking Files Integrity: END ==========")
	//--

	//--
	dtObjUtc := smart.DateTimeStructUtc("")
	var theDbName = PG_DB + "_" + dtObjUtc.Years + "_" + dtObjUtc.Months + "_" + dtObjUtc.Days
	log.Println("[NOTICE] ========== DB Restore: START [" + theDbName + "] ==========")
	//--
	var theCreateDbSQL string = "CREATE DATABASE " + theDbName + " OWNER " + PG_USER + " ENCODING 'UTF8' LC_COLLATE 'C' TEMPLATE " + PG_TEMPLATE_DB + ";"
	//--
	var theSetDbOwnerSQL string = "ALTER SCHEMA public OWNER TO " + PG_USER + ";"
	var theRefreshMaterializedViews string = "REFRESH MATERIALIZED VIEW website.v_html_sitemap_apps; REFRESH MATERIALIZED VIEW website.v_html_sitemap_developers; REFRESH MATERIALIZED VIEW website.v_xml_sitemap_developers;"
	//--
	dbMasterConn, errMasterConn := sqlx.Connect("postgres", "host=" + PG_HOST + " port=" + PG_PORT + " user=" + PG_USER + " password=" + PG_PASS + " dbname=" + PG_MASTER_DB + " sslmode=disable")
	if(errMasterConn != nil) {
		return "Failed to Connect to PostgreSQL Server / Master DB: " + errMasterConn.Error(), archTestFile, theDbName
	} //end if
	defer dbMasterConn.Close()
	//--
	resultDbExists := []DbExists{}
	err := dbMasterConn.Select(&resultDbExists, "SELECT 1 AS the_db_exists FROM pg_database WHERE datname = $1 LIMIT 1 OFFSET 0", theDbName)
	if(err != nil) {
		return "The PostgreSQL Server DB returned an Error when trying to check if DB exists: " + err.Error(), archTestFile, theDbName
	} //end if
	if(len(resultDbExists) > 0) {
		if(resultDbExists[0].Exists == "1") {
			return "The PostgreSQL Server DB `" + theDbName + "` already exists ...", archTestFile, theDbName
		} //end if
	} //end if
	//--
	_, errCreateDb := dbMasterConn.Exec(theCreateDbSQL)
	if(errCreateDb != nil) {
		return "DB Create ERRORS: " + errCreateDb.Error(), archTestFile, theDbName
	} //end if
	//--
	dbConn, errDbConn := sqlx.Connect("postgres", "host=" + PG_HOST + " port=" + PG_PORT + " user=" + PG_USER + " password=" + PG_PASS + " dbname=" + theDbName + " sslmode=disable")
	if(errDbConn != nil) {
		return "Failed to Connect to PostgreSQL Server / DB [" + theDbName + "]: " + errDbConn.Error(), archTestFile, theDbName
	} //end if
	defer dbConn.Close()
	//--
	_, errSetPrivDb := dbConn.Exec(theSetDbOwnerSQL)
	if(errSetPrivDb != nil) {
		return "DB Set Privileges ERRORS: " + errSetPrivDb.Error(), archTestFile, theDbName
	} //end if
	//--
	isRestoreSchemaSuccess, _, errRestoreSchemaStd := smart.ExecTimedCmd(CMD_TIMEOUT, "output", "capture+output", "PGPASSWORD=" + PG_PASS, "", PG_PSQL_EXECUTABLE, "--quiet", "--echo-errors", "--no-readline", "--host=" + PG_HOST, "--port=" + PG_PORT, "--username=" + PG_USER, "--no-password", "--dbname=" + theDbName, "--file=" + BKP_UNARCHIVE_FOLDER + "db-schema.sql")
//	if((isRestoreSchemaSuccess != true) || (errRestoreSchemaStd != "")) {
	if(isRestoreSchemaSuccess != true) { // ignore the errStd here ... Ex: `ERROR:  unrecognized configuration parameter "default_table_access_method"`
		return "DB Restore FAILED (Schema): Errors / StdErr:\n`" + errRestoreSchemaStd + "`\n", archTestFile, theDbName
	} //end if
	//--
	isRestoreDataSuccess, _, errRestoreDataStd := smart.ExecTimedCmd(CMD_TIMEOUT, "output", "capture+output", "PGPASSWORD=" + PG_PASS, "", PG_PSQL_EXECUTABLE, "--quiet", "--echo-errors", "--no-readline", "--host=" + PG_HOST, "--port=" + PG_PORT, "--username=" + PG_USER, "--no-password", "--dbname=" + theDbName, "--file=" + BKP_UNARCHIVE_FOLDER + "db-data.sql")
	if((isRestoreDataSuccess != true) || (errRestoreDataStd != "")) {
		return "DB Restore FAILED (Data): Errors / StdErr:\n`" + errRestoreDataStd + "`\n", archTestFile, theDbName
	} //end if
	//--
	log.Println("[NOTICE] ========== DB Restore: END [" + theDbName + "] ==========")
	//--

	//--
	_, errRefreshMatViews := dbConn.Exec(theRefreshMaterializedViews)
	if(errRefreshMatViews != nil) {
		return "DB Set Privileges ERRORS: " + errRefreshMatViews.Error(), archTestFile, theDbName
	} //end if
	//--

	//--
	doneFileWrOk, doneFileWrErr := smart.SafePathFileWrite(archDoneFile, "w", false, "DONE: " + smart.DateNowUtc())
	if((doneFileWrOk != true) || (doneFileWrErr != "")) {
		return "FAILED to create the Control File (" + archDoneFile + ") as: `" + doneFileWrErr + "`\n", archTestFile, theDbName
	} //end if
	//--
	log.Println("[DEBUG] ## DONE: Control File Created as: `" + archDoneFile + "` ##")
	//--

	//--
	var DateTimeEndUtc string = smart.DateNowUtc()
	//--
	log.Println("[OK] ### PgSQL Restore COMPLETED :: " + DateTimeEndUtc + " ; DataBase: `" + theDbName + "` ###")
	//--

	//--
	return "", archTestFile, theDbName
	//--

} //END FUNCTION


//---


// #END

