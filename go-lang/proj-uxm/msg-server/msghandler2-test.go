
package main

// msghandler2.go
// r.20221018.1858 :: STABLE

import (
	smart "github.com/unix-world/smartgo"

	msgpaksrv "github.com/unix-world/smartgoext/websocketmsgpak"
)

const BKP_RESTORE_FOLDER string = "downloads/"

//=======

//var srvAllowedHttpCustomCmds map[string]bool = nil
var srvAllowedHttpCustomCmds map[string]bool = map[string]bool{
	"ABC": true, // if true can be schedduled also via HTTP(S) tasks manager, else only by cron tasks manager
	"ABCD": false,
}

func initServerInternalTasks(webDavUrl string, intervalMsgSeconds uint32, cliTlsMode string, srvHttpAddr string, srvHttpPort uint16, authUsername string, authPassword string) {
	// nothing
}

var cronTasks []msgpaksrv.CronMsgTask
func initServerMsgPakClientTasks() {
	cronTasks = []msgpaksrv.CronMsgTask{ // list of schedduled commands to send to clients
		msgpaksrv.CronMsgTask{ "* * * * *",   "ABC", "DEF", }, 		// run every minute
		msgpaksrv.CronMsgTask{ "*/2 * * * *", "ABCD", "EFGH", }, 	// run every 2 minutes
	}
}

var uxmIsPgBackupTaskRunning bool = false
func RunTaskPgBackup() string {
	return "ERR: Not Implemented ... this is only a sample ..."
}

var uxmIsPgRestoreTaskRunning bool = false
func RunRestoreTask() (string, string, string) {
	return "", "", ""
}

func CustomMessageHandler(isServer bool, id string, remoteId string, cmd string, data string, authUsername string, authPassword string) (string, string) {
	//--
	defer smart.PanicHandler()
	//--
	var answerMsg string = ""
	var answerData string = ""
	//--
	if(isServer == true) {
		//--
		switch(cmd) {
			case "<DEF>":
				answerMsg = "<OK:DEF>"
				answerData = "0123456789"
			case "<OK:ABC>":
				// skip answer
				break
			default: // unhandled
				answerMsg = "<ERR:UNHANDLED> No Server Handler for command: `" + cmd + "` ..." // return an error answer
		} //end switch
		//--
	} else { // client
		//--
		switch(cmd) {
			case "<ABC>":
				answerMsg = "<OK:ABC>"
				answerData = "This is a sample ..."
				msgpaksrv.MsgPakSetClientTaskCmd("<DEF>", "sample ...")
				break
			case "<OK:DEF>":
				// skip answer
				break
			default: // unhandled
				answerMsg = "<ERR:UNHANDLED> No Client Handler for command: `" + cmd + "` ..." // return an error answer
		} //end switch
		//--
	} //end if else
	//--
	return answerMsg, answerData
	//--
} //END FUNCTION


//=======


// #END
