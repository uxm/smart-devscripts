
package main

// client2.go
// r.20221018.1858 :: STABLE

import (
	"log"
	"time"

	smart 		"github.com/unix-world/smartgo"
	msgpaksrv 	"github.com/unix-world/smartgoext/websocketmsgpak"
	crontab 	"github.com/unix-world/smartgoext/crontab"
)


func main() {

	//--

	LogToConsoleWithColors()

	smart.HandleAbortCtrlC(msgpaksrv.WAIT_CLOSE_LIMIT_SECONDS)

	settings, iniErr = GetIniSettings(false) // client
	if((settings == nil) || (iniErr != "")) {
		FatalError("Settings Load FAILED # " + iniErr)
		return
	} //end if

	LogSetByIni(settings["log:Level"], settings["log:Format"])

	//--

	var authUsername string = settings["auth:Username"]
	var authPassword string = settings["auth:Password"]
	var sharedEncPrivKey string = settings["auth:SharedEncPrivKey"]

	var intervalMsgSeconds uint32 = uint32(smart.ParseStrAsUInt64(settings["msgpak:IntervalMsgSeconds"]))
	var wdavTimeoutSeconds uint32 = uint32(smart.ParseStrAsUInt64(settings["webdav:TimeoutSeconds"]))
	if(wdavTimeoutSeconds <= 0) {
		log.Println("[ERROR] WebDAV Timeout is Zero or less:", wdavTimeoutSeconds)
	} //end if

	var cliTlsMode string = settings["client:TlsMode"]
	var theServerAddr string = settings["server:HttpAddr"] + ":" + settings["server:HttpPort"]
	var serverPool = []string{theServerAddr} // example: []string{"127.0.0.1:8887", "127.0.0.2:8887"}

	var clientID string = msgpaksrv.MsgPakGenerateUUID()

	ctab := crontab.New()
	cronJoberr := ctab.AddJob("*/15 * * * *", func(){ // every 15 mins (for tests)
		if(!smart.PathExists(BKP_RESTORE_FOLDER)) {
			log.Println("[NOTICE] ······· ······· SKIP Running the Client Task via Self-Cron Job: PgRestore (nothing to do) ·······")
		} else if(uxmIsPgRestoreTaskRunning != false) {
			log.Println("[NOTICE] ······· ······· SKIP Running the Client Task via Self-Cron Job: PgRestore (the task is already running in background) ·······")
		} else {
			log.Println("[NOTICE] ······· ······· Running the Server Task via Self-Cron Job: PgRestore ·······")
			errRestore, archFile, dbName := RunRestoreTask()
			if(errRestore != "") {
				log.Println("[ERROR] PgRestore [" + dbName + "] Cron Task Failed for `" + archFile + "` # " + errRestore)
			} else {
				smart.SafePathDirDelete(BKP_RESTORE_FOLDER, false)
				log.Println("[OK] PgRestore [" + dbName + "] Cron Task Completed for `" + archFile + "`")
				msgpaksrv.MsgPakSetClientTaskCmd("<OK:DB.RESTORE>", "[data]" + "\n" + "client = " + clientID + "\n" + "archive = " + smart.PathBaseName(archFile) + "\n" + "dbname = " + dbName + "\n")
			} //end if else
		} //end if
	})
	if(cronJoberr == nil) {
		log.Println("[INFO] Registering the `PgRestore` Cron Task")
	} else {
		log.Println("[ERROR] Failed to Register the `PgRestore` Cron Task")
		return
	} //end if

	go msgpaksrv.MsgPakClientRun(clientID, serverPool, cliTlsMode, msgpaksrv.CERTIFICATES_DEFAULT_PATH, authUsername, authPassword, sharedEncPrivKey, intervalMsgSeconds, CustomMessageHandler)

	for { // infinite loop
		time.Sleep(time.Duration(60) * time.Second)
		log.Println("### MessagePack Client is Running with Servers Pool", serverPool)
	} //end for

} //END FUNCTION

// #END
