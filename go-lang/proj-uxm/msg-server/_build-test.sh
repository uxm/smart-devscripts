#!/bin/sh

# r.20221018.1858 :: STABLE

if [ -f server-test ]; then
    rm server-test
fi

if [ -f client-test ]; then
    rm client-test
fi

go build -o server-test server2.go shared2.go msghandler2-test.go
go build -o client-test client2.go shared2.go msghandler2-test.go

echo "======= Build DONE: Server and Client [TEST]"
sleep 2
