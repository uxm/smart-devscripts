
// GO Lang
// go build test-webview.go ; on openbsd may need to: CGO_LDFLAGS_ALLOW='-Wl,-z,wxneeded|-Wl,-rpath-link,/usr/X11R6/lib' go build wifi-manager.go
// (c) 2020-2022 unix-world.org
// version: r.20220405.0521

package main

import (
	"os"
	"log"

	"encoding/hex"
	"crypto/sha1"
	"golang.org/x/crypto/pbkdf2"

	smart "github.com/unix-world/smartgo"
	assets "github.com/unix-world/smartgo/web-assets"
	webview "github.com/unix-world/smartgo/webview2"
//	webview "github.com/zserge/webview2"
)


const (

	THE_VERSION = "r.20220405.0521"

	WIFI_INTERFACE = "iwx0"

	TPL_CSS = `<style>
div#growl {
	width: 250px;
	position: fixed;
	top: 5px;
	right: 5px;
	padding: 5px;
	padding-left: 10px;
	padding-right: 10px;
	background: #003366;
	border-radius: 5px;
	opacity: 0.95;
	line-height: 36px;
	font-size: 28px;
}
div#growl * {
	color: #FFFFFF;
}
div#growl span.gtxt {
	display: inline-block;
	margin-top:  7px;
	margin-left: 5px;
	float: right;
}
div#growl img {
	display: inline-block;
	margin-top: 10px;
	width: 32px;
	height: 32px;
}
div.text-ok, div.text-err {
	background: #ECECEC;
	border: 1px solid #CCCCCC;
}
div.text-ok * {
	font-size: 1rem;
	font-weight: bold;
	color: #006699;
}
div.text-err * {
	font-size: 1rem;
	font-weight: bold;
	color: #FF5500;
}
div.text-ok pre, div.text-err pre {
	overflow-x: auto;
	padding-left:  10px;
	padding-right: 10px;
}

form.settings-form label {
	font-weight: bold;
}
form.settings-form input,
form.settings-form select {
	width: 500px;
}
</style>
`

	TPL_JS = `<script>
function getFormInputValue(id) {
	var val = '';
	try {
		var el = document.getElementById(id);
		val = el.value;
	} catch(err) {
		val = '';
	}
	return String(val);
}
function getFormSelectValue(id) {
	var val = '';
	try {
		var el = document.getElementById(id);
		val = el.options[el.selectedIndex].value;
	} catch(err) {
		val = '';
	}
	return String(val);
}
</script>
<script>
function call__quit(){
	goQuit(); // go RPC
}
function call__home(){
	goHome(); // go RPC
}
function call__scan(){
	goScan(); // go RPC
}
function call__off(){
	goOff(); // go RPC
}
function call__off(){
	goOn(); // go RPC
}
function call__settform() {
	goSettForm(); // go RPC
}
function call__settapply() {
	var ssid = getFormInputValue('ssid');
	var mode = getFormSelectValue('mode');
	var proto = getFormSelectValue('proto');
	var cipher = getFormSelectValue('cipher');
	var auth = getFormSelectValue('auth');
	var pass = getFormInputValue('pass');
	goSettApply(ssid, mode, proto, cipher, auth, pass); // go RPC
}
</script>
`

	TPL_FOOTER = `<div style="text-align:right; color:#778899;">
<small>OpenBSD Wifi Manager ` + THE_VERSION + ` &copy;&nbsp;2020-2022&nbsp;unix-world.org</small>
</div>
`

	TPL_HOME_OK = `
<button class="ux-button ux-button-dark" onClick="call__quit();">Quit</button>
<!-- <button class="ux-button ux-button-special" onClick="call__off();">Turn OFF Wifi</button> -->
<button class="ux-button ux-button-primary" onClick="call__scan();">Scan Wifi Networks</button>
<hr>
<h1>[###AREA-TTL|html###]</h1>
<div id="ok" class="text-ok"><pre>
[###OUTPUT|html###]
</pre></div>
<script>
setTimeout(function(){ call__home(); }, 15500);
</script>
<div id="growl">
<img src="data:image/svg+xml,[###SVG-IMG|url|html###]"><span class="gtxt">Refreshing ...</span>
</div>
`

	TPL_HOME_ERR = `
<!-- <button class="ux-button ux-button-regular" onClick="call__on();">Turn ON Wifi</button> -->
<hr>
<h1>[###AREA-TTL|html###]</h1>
<div id="ok" class="text-ok"><pre>
[###OUTPUT|html###]
</pre></div>
<br>
<div id="err" class="text-err"><pre>
[###ERRORS|html###]
</pre></div>
`

	TPL_SCAN_OK = `
<button class="ux-button" onClick="call__home();">Go Back</button>
<button class="ux-button ux-button-info" onClick="call__settform();">Connect to a Wifi Network</button>
<hr>
<h1>[###AREA-TTL|html###]</h1>
<div id="ok" class="text-ok"><pre>
[###OUTPUT|html###]
</pre></div>
<script>
setTimeout(function(){ call__scan(); }, 30250);
</script>
<div id="growl">
<img src="data:image/svg+xml,[###SVG-IMG|url|html###]"><span class="gtxt">Scanning ...</span>
</div>
`

	TPL_SCAN_ERR = `
<button class="ux-button" onClick="call__home();">Go Back</button>
<hr>
<h1>[###AREA-TTL|html###]</h1>
<div id="ok" class="text-ok"><pre>
[###OUTPUT|html###]
</pre></div>
<br>
<div id="err" class="text-err"><pre>
[###ERRORS|html###]
</pre></div>
<h3>Scanning ERROR !</h3>
`

	TPL_SETT_FORM = `
<button class="ux-button" onClick="call__home();">Go Back</button>
<hr>
<h1>[###AREA-TTL|html###]</h1>
<form id="settings" class="ux-form ux-form-aligned settings-form">
	<fieldset>
		<div class="ux-control-group">
			<label for="ssid">Network Name</label>
			<input id="ssid" type="text" placeholder="Network Name (SSID)" maxlength="32">
		</div>
		<div class="ux-control-group">
			<label for="mode">Mode</label>
			<select id="mode">
				<option value="11ac">802.11ac (1300 Mbps, 5Ghz only)</option>
				<option value="11ax">802.11ax (1300 Mbps, 2.4Ghz and 5Ghz)</option>
				<option value="11n" selected>802.11n (450 Mbps, 2.4Ghz and 5Ghz)</option>
				<option value="11g">802.11g (54 Mbps, 2.4Ghz only)</option>
				<option value="11a">802.11a (54 Mbps, 5Ghz only)</option>
				<option value="11b">802.11b (11 Mbps, 2.4Ghz only)</option>
			</select>
		</div>
		<div class="ux-control-group">
			<label for="proto">Protocol</label>
			<select id="proto">
				<option value="wpa2" selected>WPA2</option>
				<option value="wpa1">WPA1 (weak)</option>
			</select>
		</div>
		<div class="ux-control-group">
			<label for="cipher">Cipher</label>
			<select id="cipher">
				<option value="ccmp" selected>CCMP (strong, AES)</option>
				<option value="tkip">TKIP (weak)</option>
			</select>
		</div>
		<div class="ux-control-group">
			<label for="auth">Authentication</label>
			<select id="auth">
				<option value="psk" selected>WPA / PSK</option>
			</select>
		</div>
		<div class="ux-control-group">
			<label for="pass">Passphrase</label>
			<input id="pass" type="password" placeholder="Passphrase for WPA/PSK mode" maxlength="63">
		</div>
	</fieldset>
	<button class="ux-button ux-button-highlight" onClick="call__settapply(); return false;">Apply Wifi Connection</button>
</form>
`

	TPL_SETT_APPLY_OK = `
<button class="ux-button" onClick="call__home();">Go Back</button>
<hr>
<h1>[###AREA-TTL|html###]</h1>
<div id="ok" class="text-ok"><pre>
[###OUTPUT|html###]
</pre></div>
`

	TPL_SETT_APPLY_ERR = `
<button class="ux-button" onClick="call__home();">Go Back</button>
<hr>
<h1>[###AREA-TTL|html###]</h1>
<div id="ok" class="text-ok"><pre>
[###OUTPUT|html###]
</pre></div>
<br>
<div id="err" class="text-err"><pre>
[###ERRORS|html###]
</pre></div>
`

)


func LogToConsoleWithColors() {
	//--
	smart.ClearPrintTerminal()
	//--
	smart.LogToConsole("DEBUG", true)
	//--
} //END FUNCTION


func fatalError(logMessages ...interface{}) {
	//--
	log.Fatal("[ERROR] ", logMessages) // standard logger
	//--
	os.Exit(1)
	//--
} //END FUNCTION


func getErrFromCmdStdErr(stdErr string) string {
	//--
	return smart.StrTrimWhitespaces(smart.StrReplaceAll(stdErr, "[SmartGo:cmdExec:Exit:ERROR]", ""))
	//--
} //END FUNCTION


func renderTplDoc(areaTitleTxt string, areaMainHtml string) string {
	//--
	return assets.HtmlStandaloneTemplate(areaTitleTxt, TPL_CSS + TPL_JS, "<br>" + areaMainHtml + "\n" + TPL_FOOTER)
	//--
} //END FUNCTION


func renderHome() string {
	//--
	isSuccess, outStd, errStd := smart.ExecTimedCmd(30, "capture", "capture", "", "", "doas", "ifconfig", WIFI_INTERFACE)
	//--
	var arr = map[string]string{
		"AREA-TTL": 	"Wifi Status " + smart.DateNowIsoLocal(),
		"OUTPUT": 		outStd,
		"ERRORS": 		getErrFromCmdStdErr(errStd),
		"SVG-IMG": 		assets.ReadWebAsset("lib/framework/img/loading-spin.svg"),
	}
	//--
	var theHtml string = ""
	//--
	if((isSuccess == true) && (errStd == "")) {
		theHtml = smart.RenderMarkersTpl(TPL_HOME_OK, arr)
	} else {
		theHtml = smart.RenderMarkersTpl(TPL_HOME_ERR, arr)
	} //end if else
	//--
	return renderTplDoc("Wifi Home", theHtml)
	//--
} //END FUNCTION


func renderScan() string {
	//--
	isSuccess, outStd, errStd := smart.ExecTimedCmd(30, "capture", "capture", "", "", "doas", "ifconfig", WIFI_INTERFACE, "scan")
	//--
	var arr = map[string]string{
		"AREA-TTL": 	"Available Wifi Networks " + smart.DateNowIsoLocal(),
		"OUTPUT": 		outStd,
		"ERRORS": 		getErrFromCmdStdErr(errStd),
		"SVG-IMG": 		assets.ReadWebAsset("lib/framework/img/loading-spin.svg"),
	}
	//--
	var theHtml string = ""
	//--
	if((isSuccess == true) && (errStd == "")) {
		theHtml = smart.RenderMarkersTpl(TPL_SCAN_OK, arr)
	} else {
		theHtml = smart.RenderMarkersTpl(TPL_SCAN_ERR, arr)
	} //end if else
	//--
	return renderTplDoc("Wifi Scan", theHtml)
	//--
} //END FUNCTION


func renderSettingsForm() string {
	//--
	var arr = map[string]string{
		"AREA-TTL": 	"Connect to a Wifi Network (DHCP) - Form",
	}
	//--
	var theHtml string = smart.RenderMarkersTpl(TPL_SETT_FORM, arr)
	//--
	return renderTplDoc("Wifi Form", theHtml)
	//--
} //END FUNCTION


func renderSettingsApply(ssid string, mode string, proto string, cipher string, auth string, pass string) string {
	//--
	rawKey := pbkdf2.Key([]byte(pass), []byte(ssid), 4096, 32, sha1.New)
	var hexKey string = "0x" + smart.StrToLower(hex.EncodeToString(rawKey))
	//--
	log.Println("[DEBUG] SettingsApply ; SSID=`" + ssid + "` ; HexKey=`" + hexKey + "` ; Mode=`" + mode + "` ; Proto=`" + proto + "` ; Cipher=`" + cipher + "` ; Auth=`" + auth + "`")
	//--
	var arr = map[string]string{
		"AREA-TTL": 	"Connect to a Wifi Network (DHCP) - Apply",
		"OUTPUT": 		"",
		"ERRORS": 		"",
	}
	//--
	var theHtml string = ""
	//--
	if((ssid == "") || (smart.StrTrimWhitespaces(ssid) == "")) {
		arr["ERRORS"] = "ERROR: Empty SSID ..."
		theHtml = smart.RenderMarkersTpl(TPL_SETT_APPLY_ERR, arr)
		return renderTplDoc("Wifi Connect", theHtml)
	} //end if
	//--
	isSuccess, outStd, errStd := smart.ExecTimedCmd(30, "capture", "capture", "", "", "doas", "ifconfig", WIFI_INTERFACE, "mode", mode, "wpa", "wpaakms", auth, "wpaprotos", proto, "wpagroupcipher", cipher, "nwid", ssid, "wpakey", hexKey, "autoconf")
	//--
	arr["OUTPUT"] = outStd;
	arr["ERRORS"] = getErrFromCmdStdErr(errStd)
	//--
	if((isSuccess == true) && (errStd == "")) {
		theHtml = smart.RenderMarkersTpl(TPL_SETT_APPLY_OK, arr)
	} else {
		theHtml = smart.RenderMarkersTpl(TPL_SETT_APPLY_ERR, arr)
	} //end if else
	//--
	return renderTplDoc("Wifi Connect", theHtml)
	//--
} //END FUNCTION


//---


func makeDataUrl(htmlDoc string) string {
	//--
//	return `data:text/html,` + smart.RawUrlEncode(htmlDoc)
	return `data:text/html;base64,` + smart.Base64Encode(htmlDoc)
	//--
} //END FUNCTION


//---


func init() {

	LogToConsoleWithColors()

} //END FUNCTION


func main() {

	w := webview.New(false)
	defer w.Destroy()
	w.SetTitle("OpenBSD Wifi Manager")
	w.SetSize(980, 720, webview.HintFixed)

	w.Bind("goQuit", func() {
		w.Terminate()
	})
	w.Bind("goHome", func() {
		log.Println("[NOTICE] navigate Home")
		w.Navigate(makeDataUrl(renderHome()))
	})
	w.Bind("goScan", func() {
		log.Println("[NOTICE] navigate Scan")
		w.Navigate(makeDataUrl(renderScan()))
	})
	w.Bind("goSettForm", func() {
		log.Println("[NOTICE] navigate SettingsForm")
		w.Navigate(makeDataUrl(renderSettingsForm()))
	})
	w.Bind("goSettApply", func(ssid string, mode string, proto string, cipher string, auth string, pass string) {
		log.Println("[NOTICE] navigate SettingsApply")
		w.Navigate(makeDataUrl(renderSettingsApply(ssid, mode, proto, cipher, auth, pass)))
	})

	log.Println("[NOTICE] navigate Home")
	w.Navigate(makeDataUrl(renderHome()))

	w.Run()

} //END FUNCTION


// #END
