
// small webdav server written in go
// (c) 2020-2023 unix-world.org
// r.20230122.0126 :: STABLE

package main

import (
	"os"
	"os/signal"
	"syscall"
	"time"
	"log"
	"flag"

	smart "github.com/unix-world/smartgo"
	wdavsrv "github.com/unix-world/smartgo/webdavsrv"
)


const (
	ALLOWED_IP_LIST = "" // "<127.0.0.1>,<127.0.0.2>" or "<127.0.0.1>" or ""
	AUTH_USERNAME = "admin"
	AUTH_PASSWORD = "the-pass"

	TIMEOUT_SECONDS uint32 = 7200
)


func LogToConsoleWithColors() {
	//--
	smart.ClearPrintTerminal()
	//--
//	smart.LogToStdErr("DEBUG")
	smart.LogToConsole("DEBUG", true) // colored or not
//	smart.LogToFile("WARNING", "logs/", "json", true, true) // json | plain ; also on console ; colored or not
	//--
//	log.Println("[DATA] Data Debugging")
//	log.Println("[DEBUG] Debugging")
//	log.Println("[NOTICE] Notice")
//	log.Println("[WARNING] Warning")
//	log.Println("[ERROR] Error")
//	log.Println("[OK] OK")
//	log.Println("A log message, with no type") // aka [INFO]
	//--
} //END FUNCTION


func main() {

	//--

	LogToConsoleWithColors()

	//--

	c := make(chan os.Signal, 2)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		log.Println("[NOTICE] Execution Aborted")
		os.Exit(1)
	}()

	//--

	httpAddr := flag.String("h", wdavsrv.SERVER_ADDR, "HTTP Host to listen to. 127.0.0.1 will listen only on localhost or or a local or internet valid IPv4")
	httpPort := flag.Uint("p", uint(wdavsrv.SERVER_PORT), "HTTP Port to listen to")
	serveMode := flag.String("s", "https", "Can be set to https or http. By default is: https")

	flag.Parse()

	if(*httpPort > 65535) {
		*httpPort = 0
	} //end if

	var serveSecure bool = true
	if(*serveMode == "http") {
		serveSecure = false
	} //end if

	go wdavsrv.WebdavServerRun(wdavsrv.STORAGE_DIR, serveSecure, wdavsrv.CERTIFICATES_DEFAULT_PATH, *httpAddr, uint16(*httpPort), TIMEOUT_SECONDS, ALLOWED_IP_LIST, AUTH_USERNAME, AUTH_PASSWORD)

	for i := 0; i >= 0; i++ { // infinite loop
		time.Sleep(time.Duration(60) * time.Second)
		log.Println("[INFO] WebDAV Service Running since", i + 1, "minutes at", *httpAddr, "on port", uint16(*httpPort), "HTTPS:", serveSecure)
	} //end for

} //END FUNCTION


// #end
