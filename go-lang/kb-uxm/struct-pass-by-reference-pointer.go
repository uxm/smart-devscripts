// Out-of-the-box Go gives us 2 ways to initialize structs - struct literals and the `new` build-in function.
// Let’s see what these look like for a simple struct named Person:

package person

import (
	"log"
)

type Person struct {
	age  int
	name string
}

// A pointer is a variable which stores the address of another variable.
// A reference is a variable which refers to another variable.

// References are usually preferred over pointers whenever you don't need “reseting”.
// This usually means that references are most useful in a class's public interface.
// References typically appear on the skin of an object, and pointers on the inside.

// A reference variable provides a new name to an existing variable.
// It is dereferenced implicitly and does not need the dereferencing operator * to retrieve the value referenced.
// On the other hand, a pointer variable stores an address. You can change the address value stored in a pointer.

// Should you use pointers in Golang ?
// However, in many cases, you will indeed need a pointer.
// The problem is, Golang being Golang, you are passing by value, meaning you are not passing the real p but only a clone. That copies it's values.
// Changing the state of the clone will not affect the original. This is why you need a pointer here.

// What is the use of * in Golang ?
// In Go a pointer is represented using the * (asterisk) character followed by the type of the stored value.
// In the zero function xPtr is a pointer to an int . * is also used to “dereference” pointer variables.

// Address of Operator (&)
// Indirection Operator (*)

// What does * do to a pointer ?
// Note: The notation can be a little confusing.
// If you see the * in a declaration statement, with a type in front of the *, a pointer is being declared for the first time.
// AFTER that, when you see the * on the pointer name, you are dereferencing the pointer to get to the target.

// What does the (*) asterisk do in "Go"?
// * attached to a type (*string) indicates a pointer to the type. May be also in the front of struct like *Person and so on
// * attached to a variable in an assignment (*v = ...) indicates an indirect assignment. That is, change the value pointed at by the variable.
// * attached to a variable or expression (*v) indicates a pointer dereference. That is, take the value the variable is pointing at.

// What does the (&) amp do in "Go"?
// & attached to a variable or expression (&v) indicates a reference. That is, create a pointer to the value of the variable or to the field.

func LogPersons() {

	var s string
	s = "hello"
	// make p type pointer (to string only) and assign value to address of s
	var p *string = &s // type *string
	log.Println("p", p)
	// or
	q := &s // shorthand, same deal
	log.Println("q", q)

	// struct literal, pointer (memory location)
	person1 := &Person{
		age:  25,
		name: "Steve",
	}

	// new returns a pointer (memory location) to the struct ; each individual field inside the struct can then be accessed
	// and its value can be initialized or manipulated
	// Note: another way of instantiating a struct in Golang is by using the pointer address operator.
	// An example of the usage of the new keyword is shown below.
	// AVOID USE THIS SYNTAX, BECAUSE IS TRICKY: MOST OF OTHER LANGUAGES USE NEW TO CLONE

	// new build-in
	person2 := new(Person)
	person2.age = 25
	person2.name = "Steve"

	log.Println(person1, person2)

}

// #end
