// struct copy vs new

package person

import (
	"fmt"
)

type Person struct {
	Name string
	Age  int
}

func LogPersons() {

	// copy (clone)
	alice1 := Person{"Alice", 30}
	alice2 := alice1
	fmt.Println(alice1 == alice2)   // => true, they have the same field values
	fmt.Println(&alice1 == &alice2) // => false, they have different memory addresses
	alice2.Age += 10
	fmt.Println(alice1 == alice2) // => false, now they have different field values

}

// #end
